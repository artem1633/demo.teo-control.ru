<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'defaultRoute' => 'site/dashboard',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'pAqZRZ1cnXj49zV9Xkl640pmQ4aMShMa',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/views/yii2-app',
                    '@vendor/teo_crm/yii2-comments/widgets/views' => '@app/views/comments',
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.mail.ru',
                'username' => 'zvonki.crm@mail.ru',
                'password' => '86JJ;37nru8Kli',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'vk' => [
            'class' => '\app\components\VK',
            'access_token' => null,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                /*'' => 'site/boards-client',
                'board-detail-client/<id:\d+>' => 'site/board-detail-client',
                'board-detail-order/<id:\d+>' => 'site/board-detail-order',
                '<action>'=>'site/<action>',*/

            ],
        ],
        'firebasePusher' => [
            'class' => 'app\components\FirebasePusher',
            'apiKey' => 'AAAAOmXc0-E:APA91bEoh0gtN2lOve_wtFYyDNuKL5Y6bh97K76GCj6IpiYM2Uo5-4naLO1hjzC7ViuVFPo2b8SKnAplcCxujVANCQIjXV83aJzUwuaLYWxSBUL4Kn2eG1fNup7mJT_pzP5itYPJ_34q',
        ],
        
    ],
    'modules' => [
        'comments' => [
            'class' => 'teo_crm\yii\module\Comments\Module',
            'userIdentityClass' => 'app\models\User',
            'useRbac' => false,
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
