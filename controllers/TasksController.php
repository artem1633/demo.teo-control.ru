<?php

namespace app\controllers;

use Yii;
use app\models\Tasks;
use app\models\TasksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Sprints;
use app\models\StagesTasks;
use app\models\UploadForm;
use app\models\Project;
use app\models\Users;
use app\models\Changing;

/**
 * TasksController implements the CRUD actions for Tasks model.
 */
class TasksController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tasks models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new TasksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Tasks model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Задача",
                    'content'=>$this->renderAjax('modals/view-ajax', [
                        'model' => $this->findModel($id),
                    ]),
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Tasks model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionEditBase($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->scenario = Tasks::SCENARIO_BASE;
        $last = $this->findModel($id);

        if ($model->load($request->post()) && $model->save()) {

            $subject = "Изменено задача";
            $proyekt = $model->project0->name;
            $sprint = $model->sprint0->name;
            $zadacha = $model->task_title;
            $id = $model->id;
            $ssilka = 'http://' . $_SERVER['SERVER_NAME'] ."/tasks/view?id=".$model->id;
            $data = $model->date_delivery;
            $meneger = $model->managerProject->fio;
            $priority = $model->priority0->name; 
            $etap = $model->stage0->name;         
            $model->setChanging($last, $model, 'Изменено');
            $change = $model->Change($last, $model);  

            if($change != ""){
                $message = 'Тема: ' . $subject . "\n" . 'Проект: ' . $proyekt . "\n" .'Спринт: ' . $sprint . "\n" . 'Задача: ' . $zadacha . "\n" . 'ИД задачи: ' . $id . "\n". 'Ссылка: ' . $ssilka . "\n" . "Дата сдачи: ". $data."\n". 'Менежер: '. $meneger . "\n" . "Приоритет: ". $priority . "\n" . "Этап: " . $etap. "\n". "Изменено: " . "\n". $change; 

                if( isset($model->managerProject->vk_id) )$model->SendSmsToVk($model->managerProject->vk_id,$message);//Менеджер проекта
                if( isset($model->executor0->vk_id) )$model->SendSmsToVk($model->executor0->vk_id,$message);//Исполнитель
                if( isset($model->project0->client0->vk_id) )$model->SendSmsToVk($model->project0->client0->vk_id,$message);//клиенту

                $company = Yii::$app->user->identity->getCompany();
                $admin = Users::find()->where(['company_id' => $company, 'type' => 1])->all();
                foreach ($admin as $value) {
                    $model->SendSmsToVk($value->vkId,$message);//администратору
                }

                $companies = Users::find()->where(['company_id' => $company, 'type' => 6])->all();
                foreach ($companies as $value) {
                    $model->SendSmsToVk($value->vkId,$message);//kompany
                }
            }

            return [
                //'forceReload'=>'#base-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Редактировать задачу «{$model->task_title}»",
                'content'=>$this->renderAjax('modals/_form_base', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }

    public function actionEditSecond($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->scenario = Tasks::SCENARIO_SECOND;
        $last = $this->findModel($id);

        if ($model->load($request->post()) && $model->save()) {

            $subject = "Изменено задача";
            $proyekt = $model->project0->name;
            $sprint = $model->sprint0->name;
            $zadacha = $model->task_title;
            $id = $model->id;
            $ssilka = 'http://' . $_SERVER['SERVER_NAME'] ."/tasks/view?id=".$model->id;
            $data = $model->date_delivery;
            $meneger = $model->managerProject->fio;
            $priority = $model->priority0->name; 
            $etap = $model->stage0->name;   
            $model->setChanging($last, $model, 'Изменено');      
            $change = $model->Change($last, $model);          

            if($change != ""){
                $message = 'Тема: ' . $subject . "\n" . 'Проект: ' . $proyekt . "\n" .'Спринт: ' . $sprint . "\n" . 'Задача: ' . $zadacha . "\n" . 'ИД задачи: ' . $id . "\n". 'Ссылка: ' . $ssilka . "\n" . "Дата сдачи: ". $data."\n". 'Менежер: '. $meneger . "\n" . "Приоритет: ". $priority . "\n" . "Этап: " . $etap. "\n". "Изменено: " . "\n". $change; 

                $model->SendSmsToVk($model->managerProject->vkId,$message);//Менеджер проекта
                $model->SendSmsToVk($model->executor0->vkId,$message);//Исполнитель
                $model->SendSmsToVk($model->project0->client0->vkId,$message);//клиенту

                $company = Yii::$app->user->identity->getCompany();
                $admin = Users::find()->where(['company_id' => $company, 'type' => 1])->all();
                foreach ($admin as $value) {
                    $model->SendSmsToVk($value->vkId,$message);//администратору
                }

                $companies = Users::find()->where(['company_id' => $company, 'type' => 6])->all();
                foreach ($companies as $value) {
                    $model->SendSmsToVk($value->vkId,$message);//kompany
                }
            }

            return [
                //'forceReload'=>'#second-pjax',
                'forceClose' => true,
            ];
        } else {
            return [
                'title'=> "Редактировать задачу «{$model->task_title}»",
                'content'=>$this->renderAjax('modals/_form_second', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default btn-sm pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-primary btn-sm','type'=>"submit"])
            ];
        }
    }

    public function actionCreate($project = null, $sprint = null, $executor = null, $manager = null, $stage = null, $url = null)
    {
        $last = new Tasks(); 
        $tip = Yii::$app->user->identity->type; 
        $tip = 1;
        if($tip == 1 | $tip == 3 | $tip == 6) 
        {
            $request = Yii::$app->request;
            $model = new Tasks(); 
            $model->manager_project = $manager;
            $model->executor = $executor;
            $model->project = $project;
            $model->sprint = $sprint;
            $model->stage = $stage;
            $modelUpload = new UploadForm();
            $post = $request->post();
      
            if($post['Tasks']['step'] == null)
            {
                $model->load($request->get());
                $model->step = 1;
                $model->scenario = Tasks::SCENARIO_BASE;
            }
            if($post['Tasks']['step'] == 1)
            {
                $model->step = 1;
                if ($model->load($request->post()) && $model->validate()) 
                {
                    $model->step = 2;
                    $model->scenario = Tasks::SCENARIO_SECOND;                
                }
                if($model->step == 1) $model->scenario = Tasks::SCENARIO_BASE;
            }
            if($post['Tasks']['step'] == 2)
            {   
                if ($model->load($request->post()) && $model->validate() && $model->save()) 
                {
                    $tempFiles = json_decode(Yii::$app->request->post()['Tasks']['tempFiles'], true);
                    $model->addFiles($tempFiles, $model->id);
                    $model->setChanging($last, $model, 'Создано');

                    $subject = "Создано новая задача";
                    $proyekt = $model->project0->name;
                    $sprint = $model->sprint0->name;
                    $zadacha = $model->task_title;
                    $id = $model->id;
                    $ssilka = 'http://' . $_SERVER['SERVER_NAME'] ."/tasks/view?id=".$model->id;
                    $data = $model->date_delivery;
                    $meneger = $model->managerProject->fio;
                    $priority = $model->priority0->name; 
                    $etap = $model->stage0->name;         

                    $message = 'Тема: ' . $subject . "\n" . 'Проект: ' . $proyekt . "\n" .'Спринт: ' . $sprint . " \n" . 'Задача: ' . $zadacha . "\n" . 'ИД задачи: ' . $id . "\n". 'Ссылка: ' . $ssilka . "\n" . "Дата сдачи: ". $data."\n". 'Менежер: '. $meneger . "\n" . "Приоритет: ". $priority . "\n" . "Этап: " . $etap;

                    $model->SendSmsToVk($model->managerProject->vkId,$message);//Менеджер проекта
                    $model->SendSmsToVk($model->executor0->vkId,$message);//Исполнитель
                    $model->SendSmsToVk($model->project0->client0->vkId,$message);//клиенту

                    $company = Yii::$app->user->identity->getCompany();
                    $admin = Users::find()->where(['company_id' => $company, 'type' => 1])->all();
                    foreach ($admin as $value) {
                        $model->SendSmsToVk($value->vkId,$message);//администратору
                    }

                    $companies = Users::find()->where(['company_id' => $company, 'type' => 6])->all();
                    foreach ($companies as $value) {
                        $model->SendSmsToVk($value->vkId,$message);//kompany
                    }
                    if($url == null) return $this->redirect(['index']);
                    else return  $this->redirect([$url]);
                }
                else 
                {
                    $model->step = 2;
                    $model->scenario = Tasks::SCENARIO_SECOND;
                }
            }
            return $this->render('create', [ 'model' => $model,'modelUpload' => $modelUpload,]);
        }
        else
        {
            //\Yii::$app->session->setFlash('error', 'У вас нет прав');
            if($url == null) return $this->redirect(['index']);
            else return  $this->redirect([$url]);
        }
    }

    public function actionLists($id)
    {
        $counts = Sprints::find()->where(['communications_project' => $id])->count();  
        $datas = Sprints::find()->where(['communications_project' => $id])->all();

        if($counts > 0){
            echo "<option value = ''>Выберите спринт</option>" ;
            foreach($datas as $data){
                echo "<option value = '".$data->id."'>".$data->name."</option>" ;
            }            
        }
        
    }

    public function actionLists1($id)
    {
        $counts = StagesTasks::find()->where(['communication_id' => $id])->count();  
        $datas = StagesTasks::find()->where(['communication_id' => $id])->all();

        if($counts > 0){
            echo "<option value = ''>Выберите этап</option>" ;
            foreach($datas as $data){
                echo "<option value = '".$data->id."'>".$data->name."</option>" ;
            }            
        }
        
    }

    public function actionManagerList($id)
    {
        $counts = Project::find()->where(['id' => $id])->count();  
        $datas = Project::find()->where(['id' => $id])->one();

        if($counts > 0){
            $menejer = Users::findOne($datas->responsible);
            /*echo "<option value = ''>Выберите спринт</option>" ;
            foreach($datas as $data){*/
                echo "<option value = '".$menejer->id."'>".$menejer->fio."</option>" ;
            //}            
        }
        
    }

    public function actionExecutorList($id)
    {
        $counts = Project::find()->where(['id' => $id])->count();  
        $datas = Project::find()->where(['id' => $id])->one();

        if($counts > 0){
            $executor = Users::findOne($datas->executor);
            echo "<option value = '".$executor->id."'>".$executor->fio."</option>" ;
        }        
    }

    /**
     * Delete an existing Tasks model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Tasks model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Tasks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tasks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tasks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
    public function actionDownload($path)
    {
        return \Yii::$app->response->sendFile($path);
    }
}
