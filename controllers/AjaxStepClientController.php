<?php

namespace app\controllers;

use Yii;
use app\models\StepClient;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\StagesTasks;
use app\models\Sprints;

/**
 * AjaxStepClientController implements the CRUD actions for StepClient model.
 */
class AjaxStepClientController extends \yii\web\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
// на разделе Доски проектов создаются новые этапы задач
    public function actionCreate1($boardId)
    {
        $model = new StagesTasks();
        $sprint = Sprints::find()->where(['id' => $boardId])->one();
        $model->communication_id = $sprint->communications_project;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        } else {
            return $this->renderAjax('create1', [
                'model' => $model,
            ]);
        }
    }
}
