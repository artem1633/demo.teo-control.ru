<?php

namespace app\controllers;

use app\models\Clients;
use app\models\TasksFile;
use yii\filters\VerbFilter;
use Yii;
use app\models\Orders;
use app\models\Tasks;
use yii\web\UploadedFile;
use app\models\Directory;
use app\models\UploadForm;
Use yii\filters\AccessControl;
use app\models\StagesTasks;
use app\models\Users;

class AjaxController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];

    }

    //с этим экшном загружается файлы на задачи 
    public function actionUpload()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $order = new Tasks();
        $model = new UploadForm();

        $orderId = Yii::$app->request->post()['orderId'];

        if($orderId <= 0) {
            $orderId = $order->getLastId();
        }

        if (Yii::$app->request->isPost) {
            $model->anyFiles = UploadedFile::getInstances($model, 'anyFiles');
            if($path = $model->upload(new Directory(), $orderId)) {
                $response = ['path' => $path];
                return $response;
            }
        }

        $response = ['error' => 'Upload faile'];
        return $response;
    }
    //с этим экшном удаляется файлы из задачи 
    public function actionRemoveFile($id) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $file = TasksFile::findOne($id);
        if($file->delete()) {
            return true;
        } else {
            return false;
        }
    }
// с этим экшном задачи с этапа изменяется
    public function actionUpdateStepTasks($etapId, $taskId)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            if (($task = Tasks::findOne($taskId)) !== null) {
                $last = Tasks::findOne($taskId);
                //if($task->stage == $etapId) return false;
                $task->stage = $etapId;
                if($task->save()) {

                    $subject = "Изменено статус задачи";
                    //$subject = "Изменено задача";
                    $proyekt = $task->project0->name;
                    $sprint = $task->sprint0->name;
                    $zadacha = $task->task_title;
                    $id = $task->id;
                    $ssilka = 'http://' . $_SERVER['SERVER_NAME'] ."/tasks/view?id=".$task->id;
                    $data = $task->date_delivery;
                    $meneger = $task->managerProject->fio;
                    $priority = $task->priority0->name; 
                    $etap = $task->stage0->name;         
                    $change = $task->Change($last, $task);  

                    if($change != ""){
                        $message = 'Тема: ' . $subject . "%0A" . 'Проект: ' . $proyekt . "%0A" .'Спринт: ' . $sprint . "%0A" . 'Задача: ' . $zadacha . "%0A" . 'ИД задачи: ' . $id . "%0A". 'Ссылка: ' . $ssilka . "%0A" . "Дата сдачи: ". $data."%0A". 'Менежер: '. $meneger . "%0A" . "Приоритет: ". $priority . "%0A" . "Этап: " . $etap. "%0A". "Изменено: " . "%0A". $change; 

                        $task->SendSmsToVk($task->managerProject->vkId_id,$message);//Менеджер проекта
                        $task->SendSmsToVk($task->executor0->vkId_id,$message);//Исполнитель
                        $task->SendSmsToVk($task->project0->client0->vkId_id,$message);//клиенту

                        $company = Yii::$app->user->identity->getCompany();
                        $admin = Users::find()->where(['type' => 1])->all();
                        foreach ($admin as $value) {
                            $task->SendSmsToVk($value->vkId_id, $message);//администратору
                        }

                        $companies = Users::find()->where(['company' => $company, 'type' => 6])->all();
                        foreach ($companies as $value) {
                            $task->SendSmsToVk($value->vkId_id,$message);//kompany
                        }
                    }
                    $task->setChanging($last, $task, 'Изменено');               

                    return true;
                }
            }
        }

        return false;
    }
}
