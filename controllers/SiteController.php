<?php

namespace app\controllers;

use app\models\Logs;
use app\models\RegisterForm;
use app\models\ResetPasswordForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Project;
use app\models\Board;
use app\models\Sprints;
use app\models\Tasks;
use app\models\Signup;
use app\models\Users;
use app\models\Comment;
use app\models\Companies;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public $wrapperClass;

    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
                'only' => ['dashboard', 'boards-client', 'board-detail-sprint', 'boards-project', 'boards-sprint'],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['dashboard', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) { 
            return $this->redirect('site/boards-project');
        }else
        {
            return $this->redirect(['site/login']);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Регистрирует нового пользователя
     * @return string|Response
     */
    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $log = new Logs([
            'user_id' => null,
            'event_datetime' => date('Y-m-d H:i:s'),
            'event' => Logs::EVENT_USER_OPEN_REGISTRATION_PAGE,
        ]);
        $log->description = $log->generateEventDescription();
        $log->save();

        $this->layout = 'main-login';

        $model = new RegisterForm();

        if($model->load(Yii::$app->request->post()) && $model->register())
        {
            //Yii::$app->session->setFlash('register_success', 'Регистрация прошла успешно. Пожалуйста, авторизируйтесь');
            $login_model = new LoginForm();
            $login_model->username = $model->login;
            $login_model->password = $model->password;
            if ($login_model->login()) {
                Companies::setDefaultValues();
                return $this->goBack();
            }
            else return $this->redirect(['login']);
        } else {
            return $this->render('register', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return string|Response
     */
    public function actionReset()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'main-login';

        $model = new ResetPasswordForm();

        if($model->load(Yii::$app->request->post()) && $model->reset())
        {
            Yii::$app->session->setFlash('register_success', 'На вашу почту был выслан временный пароль. Воспользуйтесь им для авторизации');
            return $this->redirect(['login']);
        } else {
            return $this->render('reset', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Рабочий стол
     * @return Response|string
     */
// через этот екшн заходим на Рабочий стол
    public function actionDashboard()
    {

        if (Yii::$app->user->isGuest)   return $this->redirect(['site/login']);

        $tip = Yii::$app->user->identity->type;
        $user_id = Yii::$app->user->identity->id;

        if($tip == 5)
        {
            $all_tasks = Tasks::find()->where(['executor' => $user_id ])->all();
        }
        if($tip == 3)
        {
            $all_tasks = Tasks::find()->where(['manager_project' => $user_id])->all();
        }
        if($tip == 1 | $tip == 6)
        {
            $user = Users::findOne($user_id);
            if($user->creator != null) $all_tasks = Tasks::find()->where(['company_id' => $user->creator ])->all();
            else $all_tasks = Tasks::find()->where(['company_id' => $user_id])->all();
        }
        if($tip == 4)
        {
            $tasks = Project::find()->where(['client' => $user_id])->all();
            $result = [];
            foreach ($tasks as $value) {
                $result [] = $value->id;
            }
            $result = array_unique($result);
            $all_tasks = Tasks::find()->where(['project' => $result])->all();
        }

        
        $user = Users::findOne($user_id);
        if($tip == 3) $project = Project::find()->where(['responsible' => $user_id])->all();
        if($tip == 4) $project = Project::find()->where(['client' => $user_id])->all();
        if($tip == 5) {
            $all_tasks = Tasks::find()->where(['executor' => $user_id ])->all();
            $array = [];
            foreach ($all_tasks as $value) {
                $array[] = $value->project;
            }
            $project = Project::find()->where(['id' => array_unique($array)])->all();
        }
        if($tip == 1 | $tip == 6 | $tip == 0) $project = Project::find()->all();

        $all = 0; $worked = 0; $ready = 0;
        foreach ($project as $value) {
            $all++;
            if($value->status == 3) $worked++;
            if($value->status == 1) $ready++;
        }
        $result  = [
            'all' => $all,
            'worked' => $worked,
            'ready' => $ready,
        ]; 

        $users = Users::find()->where([ 'company_id' => Yii::$app->user->identity->getCompany() ])->all();
        $array = [];
        foreach ($users as $value) {
            $array [] = $value->id;
        }

        $comments = Comment::find()->where(['created_by' => $array ])->orderBy(['created_at' => SORT_DESC])->all();

        $comment = [];
        foreach ($comments as $value) {
            $user = Users::findOne($value->created_by);
            $task = Tasks::findOne($value->entity);
            $q=0;
            if($tip == 5 && $task->executor == $user_id) $q=1;
            if($tip == 4 && $task->project0->client == $user_id) $q=1;
            if($tip == 3 && $task->manager_project == $user_id) $q=1;
            if($tip == 1 | $tip == 6 | $tip == 0) $q=1;

            if($q==1){
                $name = 'Unknown';
                $task_title = 'Unknown';
                if($user != null) 
                {
                    if($user->type == 1) $name = 'Администратор';
                    if($user->type == 2) $name = 'Аналитик';
                    if($user->type == 3) $name = 'Менеджер проекта';
                    if($user->type == 4) $name = 'Клиент';
                    if($user->type == 5) $name = 'Программист';
                    if($user->type == 6) $name = 'Компания';
                }
                if($task != null) $task_title = $task->task_title;
                $comment [] = [
                    'user' => $name,
                    'title' => $task_title,
                    'text' => $value->text,
                    'id' => $user->id,
                ];
            }
        }  

        $all = 0; $worked = 0; $ready = 0;
        $total_hour = 0;

        foreach ($all_tasks as $task) {
            $all++;
            if($task->stage0->status == 2) $worked++;
            if($task->stage0->status == 3) $ready++;  
            if($task->stage0->status == 3) $total_hour += $task->time_fact;      
        } 
        $result1  = [
            'all' => $all,
            'worked' => $worked,
            'ready' => $ready,
        ];     

        return $this->render('dashboard', [
            'all_tasks' => $result1,
            'result' => $result,
            'comment' => $comment,
            'total_hour' => $total_hour,
            'tasks' => $all_tasks,
        ]);
    }


    /**
     * Авторизация
     * @return string|Response
     */
    public function actionSign()
    {
        $this->redirect(['register']);
        /*$model = new Signup();
        if ($model->load(Yii::$app->request->post())) {

            $user = new Users();
            $user->fio = $model->fio;
            $user->login = $model->email;
            $user->password = $model->password;
            $user->dostup = 1;
            $user->type = 6;
            if($user->save())
            {   
                $msg  = 'Заявка c сайта Control \n Название компании: '. $model->fio .' Логин: '.$model->email. ' Пароль: '. $model->password;
                $ch = curl_init('https://api.telegram.org/bot474474429:AAEDvb8ifBsjAEYcLBeCzxKsFwk-XxHpM0s/sendMessage?chat_id=247187885&parse_mode=html&text='.$msg);
                // устанавлваем даные для отправки
                curl_setopt($ch, CURLOPT_POSTFIELDS, '11111');
                // флаг о том, что нужно получить результат
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                // отправляем запрос
                $response = curl_exec($ch);
                // закрываем соединение
                curl_close($ch);

                Yii::$app->db->createCommand()->update('users', ['company_id' => $user->id , 'creator' => $user->id], [ 'id' => $user->id ])->execute();
                $sign = new LoginForm();
                $sign->username = $model->email;
                $sign->password = $model->password;

                if ($sign->login()) {
                    return $this->redirect(['/users/index']);
                }
            } 
        }

        return $this->render('signup', [
            'model' => $model,
        ]);*/
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
         Yii::$app->user->logout();

        $this->redirect(['login']);
    }

    public function actionLogout1()
    {
         Yii::$app->user->logout();

        $this->redirect(['signup']);
    }
// через этот екшн заходим на Доску проектов
    public function actionBoardsClient()
    {
        if (Yii::$app->user->isGuest)   return $this->redirect(['site/login']);
        $boards = Project::find()->all();

        return $this->render('boards-project', [
            'boards' => $boards,
        ]);
    }

/*    public function actionBoardDetailClient($id)
    {
        if (Yii::$app->user->isGuest)   return $this->redirect(['site/login']);
        $this->wrapperClass = 'desc-page';

        $boardObj = new Board();
        $board = $boardObj->getStepsWithClients($id);

        return $this->render('board-item-client', [
            'board' => $board,
            'boardId' => $id,
        ]);
    }*/
// через этот екшн заходим на Доску этапов задач
    public function actionBoardDetailSprint($sprint_id)
    {
        if (Yii::$app->user->isGuest)   return $this->redirect(['site/login']);
        $id = $sprint_id;
        $this->wrapperClass = 'desc-page';

        $boardObj = new Board();
        $board = $boardObj->getSprintsWithTasks($id);
        $count = 0;
        foreach ($board as $value) {
            $count ++;
        }

        return $this->render('board-item-sprint', [
            'board' => $board,
            'boardId' => $id,
            'count' => $count,
            'sprint_id' => $sprint_id,
        ]);
    }
// через этот екшн заходим на Доску проектов
    public function actionBoardsProject()
    {
        if (Yii::$app->user->isGuest)   return $this->redirect(['site/login']);
        $tip = Yii::$app->user->identity->type;

        if($tip == 5)
        {
            $tasks = Tasks::find()->where(['executor' => Yii::$app->user->identity->id ])->all();
            $result = [];
            foreach ($tasks as $value) {
                $result [] = $value->project;
            }
            $result = array_unique($result);
            $boards = Project::find()->orderBy(['sorting' => SORT_ASC])->where(['id' => $result, 'is_delete' => 0])->all();
        }

        if($tip == 3)
        {
            /*$tasks = Tasks::find()->where(['manager_project' => Yii::$app->user->identity->id ])->all();
            $result = [];
            foreach ($tasks as $value) {
                $result [] = $value->project;
            }
            $result = array_unique($result);*/
            $boards = Project::find()->orderBy(['sorting' => SORT_ASC])->where(['responsible' => Yii::$app->user->identity->id, 'is_delete' => 0])->all();
        }

        if($tip == 4)
        {
            $boards = Project::find()->orderBy(['sorting' => SORT_ASC])->where(['client' => Yii::$app->user->identity->id, 'is_delete' => 0 ])->all();
        }

        if($tip == 1 | $tip == 6 | $tip == 0)
        {
            $boards = Project::find()->orderBy(['sorting' => SORT_ASC])->all();
        }

        return $this->render('boards-project', [
            'boards' => $boards,
        ]);
    }
// через этот екшн заходим на Доску спринтов
    public function actionBoardsSprint($project_id)
    {
        if (Yii::$app->user->isGuest)   return $this->redirect(['site/login']);
        $tip = Yii::$app->user->identity->type;

        if($tip == 5)
        {
            $tasks = Tasks::find()->where(['executor' => Yii::$app->user->identity->id , 'project' => $project_id])->all();
            $result = [];
            foreach ($tasks as $value) {
                $result [] = $value->sprint;
            }
            $result = array_unique($result);
            
            $boards = Sprints::find()->where(['id' => $result, 'is_delete' => 0])->all();
        }

        if($tip == 4)
        {
            $boards = Sprints::find()->where(['communications_project' => $project_id, 'is_delete' => 0])->all();
        }

        if($tip == 3)
        {
            $boards = Sprints::find()->where(['communications_project' => $project_id, 'is_delete' => 0])->all();
        }

        if($tip == 1 | $tip == 6 | $tip == 0)
        {
            $boards = Sprints::find()->where(['communications_project' => $project_id, 'is_delete' => 0 ])->all();
        }

        return $this->render('boards-sprint', [
            'boards' => $boards,
            'project_id' => $project_id,
        ]);
    }

/*    public function actionBoardDetailProject($id)
     {
        if (Yii::$app->user->isGuest)   return $this->redirect(['site/login']);
        $this->wrapperClass = 'desc-page';

        $boardObj = new BoardProject();
        $sprints = $boardObj->getSprintsWithProject($id);

        return $this->render('board-item-project', [
            'sprints' => $sprints,
            'boardId' => $id,
        ]);
    }*/

    public function actionMymenu($id)
    {
       $session = Yii::$app->session;
        $menu = ($_SESSION['menu']) != null ? $_SESSION['menu'] : null;
        if($menu == null) $_SESSION['menu'] = 'small';
        else {
            if($menu == 'large') $_SESSION['menu'] = 'small';
            else $_SESSION['menu'] = 'large';
        }
    }

    public function actionAvtorizatsiya()
    {
      if(isset(Yii::$app->user->identity->id))
      {
        return $this->render('error');
      }        
       else
        {
            Yii::$app->user->logout();
            $this->redirect(['login']);
        }

    }
// через этот екшн можно войти на инструкцию
    public function actionInstruksion()
    {
        if (Yii::$app->user->isGuest)   return $this->redirect(['site/login']);
        return $this->render('instruksion', []);        
    }

}
