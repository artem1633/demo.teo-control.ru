<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Technologies;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $tip = Yii::$app->user->identity->type;
        if($tip == 1 | $tip ==2 | $tip == 6 | $tip == 0){
            $searchModel = new UsersSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else {
            return $this->redirect(['site/dashboard']);
        }
    }


    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Пользователь #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $tip = Yii::$app->user->identity->type;
        if($tip == 1 | $tip ==2 | $tip == 6 | $tip == 0)
        {
            $request = Yii::$app->request;
            $model = new Users(); 
            $technologies = new Technologies();
            $post = $request->post();

            $model->allTechnologies = ArrayHelper::map(Technologies::find()->where(['company_id' => Yii::$app->user->identity->company_id ])->all(), 'id', 'title');

            $navik = $post['Users']['selectTechnologies'];
            $values = "";
            foreach ($navik as $value) {
                $values .= $value.',';
            }
            $model->technology = $values;
            if($request->isAjax){
                /*
                *   Process for ajax request
                */
                Yii::$app->response->format = Response::FORMAT_JSON;
                if($request->isGet){
                    return [
                        'title'=> "Добавить Пользователя",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
            
                    ];         
                }else if($model->load($request->post()) && $model->save()){
                    $model->file = UploadedFile::getInstance($model, 'file');
                    if(!empty($model->file))
                    {
                        $model->file->saveAs('avatars/'.$model->id.'.'.$model->file->extension);
                        Yii::$app->db->createCommand()->update('users', ['avatar' => 'avatars/'.$model->id.'.'.$model->file->extension], [ 'id' => $model->id ])->execute();
                    }
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Добавить Пользователя",
                        'content'=>'<span class="text-success">Создание Пользователя успешно завершено</span>',
                        'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
            
                    ];         
                }else{           
                    return [
                        'title'=> "Добавить Пользователя",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
            
                    ];         
                }
            }else{
                /*
                *   Process for non-ajax request
                */
                if ($model->load($request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            }
        }else {
            return $this->redirect(['site/dashboard']);
        }       
    }

    /**
     * Updates an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $tip = Yii::$app->user->identity->type;
        if($tip == 1 | $tip ==2 | $tip == 6 | $tip == 0)
        {
            $request = Yii::$app->request;
            $model = $this->findModel($id);  
            $post = $request->post();

            $result = [];
            $tech = $model->technology;
            $strlen = strlen( $model->technology );
            $s = "";

            for( $i = 0; $i <= $strlen; $i++ ) 
            {
                $char = substr( $tech, $i, 1 );             
                if($char == ",") { $result [] = $s; $s=""; }
                else $s .= $char;
            }

            $model->selectTechnologies = $result;
            $model->allTechnologies = ArrayHelper::map(Technologies::find()->where(['company_id' => Yii::$app->user->identity->company_id ])->all(), 'id', 'title');

            $navik = $post['Users']['selectTechnologies'];
            $values = "";
            foreach ($navik as $value) {
                $values .= $value.',';
            }
            $model->technology = $values;

            if($request->isAjax){
                /*
                *   Process for ajax request
                */
                Yii::$app->response->format = Response::FORMAT_JSON;
                if($request->isGet){
                    return [
                        'title'=> "Изменить Пользователя #".$id,
                        'content'=>$this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                    ];         
                }else if($model->load($request->post()) && $model->save()){
                    $model->file = UploadedFile::getInstance($model, 'file');
                    if(!empty($model->file))
                    {
                        $model->file->saveAs('avatars/'.$model->id.'.'.$model->file->extension);
                        Yii::$app->db->createCommand()->update('users', ['avatar' => 'avatars/'.$model->id.'.'.$model->file->extension], [ 'id' => $model->id ])->execute();
                    }
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Пользователь #".$id,
                        'content'=>$this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                    ];    
                }else{
                     return [
                        'title'=> "Изменить Пользователя #".$id,
                        'content'=>$this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                    ];        
                }
            }else{
                /*
                *   Process for non-ajax request
                */
                if ($model->load($request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
            }
        }else {
            return $this->redirect(['site/dashboard']);
        } 
    }
    public function actionChange($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){

                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file))
                {
                    $model->file->saveAs('avatars/'.$model->id.'.'.$model->file->extension);
                    Yii::$app->db->createCommand()->update('users', ['avatar' => 'avatars/'.$model->id.'.'.$model->file->extension], [ 'id' => $model->id ])->execute();
                }

                return [ 'forceClose' => true ];    
            }else{
                 return [
                    'title'=> "Изменить Пользователя #".$id,
                    'size' => "large",
                    'content'=>$this->renderAjax('change', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    /**
     * Delete an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws ForbiddenHttpException if the model company id does't equals to user's company
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Users::findOne($id);

        if($model->company_id != Yii::$app->user->identity->getCompany() && Yii::$app->user->identity->isSuperAdmin() === false){
            throw new ForbiddenHttpException('Доступ запрещен');
        } else if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
