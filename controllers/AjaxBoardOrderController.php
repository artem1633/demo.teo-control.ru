<?php

namespace app\controllers;

use Yii;
use app\models\BoardOrder;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Project;
use app\models\Sprints;
use app\models\StagesTasks;
use app\models\Users;

class AjaxBoardOrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
//здесь выводиться список всех проектов
    public function actionIndex0()
    {
        $user = Users::findOne(Yii::$app->user->identity->id);
        if($user->creator != null) $boards = Project::find()->where(['company' => $user->creator ])->all();
        else $boards = Project::find()->where(['company' => Yii::$app->user->identity->id ])->all();

        return $this->renderAjax('index0', [
            'boards' => $boards,
        ]);
    }

//здесь выводиться список всех спринтов 
    public function actionIndex3()
    {Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post = Yii::$app->request->post();

        $id = $post['id'];
        $sprint = Sprints::findOne($id);

        //$boards = Sprints::find()->where(['communications_project' => $sprint->communications_project])->all();

        $user = Users::findOne(Yii::$app->user->identity->id);
        if($user->creator != null) $boards = Sprints::find()->where(['communications_project' => $sprint->communications_project, 'company' => $user->creator ])->all();
        else $boards = Sprints::find()->where(['communications_project' => $sprint->communications_project, 'company' => Yii::$app->user->identity->id ])->all();

        return $this->renderAjax('index3', [
            'boards' => $boards,
        ]);
    }
//здесь создается проект
    public function actionCreateproject()
    {
        $model = new Project();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model->CreateStage($model->id);
            //return $model;
            return $this->redirect(['/site/boards-project']);
        } else {
            return $this->renderAjax('create_project', [
                'model' => $model,
            ]);
        }
    }
//здесь создается спринт
    public function actionCreatesprint($id)
    {
        $model = new Sprints();
        $model->communications_project = $id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //return $model;
            return $this->redirect(['/site/boards-sprint', 'project_id' => $id]);
        } else {
            return $this->renderAjax('create_sprint', [
                'model' => $model,
                'id' => $id,
            ]);
        }
    }
//здесь создается этапы проектов
    public function actionCreate1($boardId)
    {
        $model = new StagesTasks();
        $sprint = Sprints::find()->where(['id' => $boardId])->one();
        $model->communication_id = $sprint->communications_project;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        } else {
            return $this->renderAjax('create1', [
                'model' => $model,
            ]);
        }
    }
}