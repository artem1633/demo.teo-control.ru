<?php

namespace app\controllers;

use Yii;
use app\models\Project;
use app\models\ProjectSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Sprints;
use app\models\StagesTasks;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::class,
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Проект #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Project model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $tip = Yii::$app->user->identity->type;
        if($tip == 1 | $tip == 6 | $tip == 0) 
        {
            $request = Yii::$app->request;
            $model = new Project();  
            $model->is_delete = 0;

            if($request->isAjax){
                /*
                *   Process for ajax request
                */
                Yii::$app->response->format = Response::FORMAT_JSON;
                if($request->isGet){
                    return [
                        'title'=> "Добавить Проекта",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
            
                    ];         
                }else if($model->load($request->post()) && $model->save()){
                    $model->CreateStage($model->id);
                    $sprint = new Sprints();
                    $sprint->name = 'Первый спринт';
                    $sprint->communications_project = $model->id;
                    $sprint->creator = Yii::$app->user->identity->id;
                    $sprint->status = 2;
                    $sprint->save();
                    
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Добавить Проекта",
                        'content'=>'<span class="text-success">Создание Проекта успешно завершено</span>',
                        'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
            
                    ];         
                }else{           
                    return [
                        'title'=> "Добавить Проекта",
                        'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
            
                    ];         
                }
            }else{
                /*
                *   Process for non-ajax request
                */
                if ($model->load($request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            }
        }
        else
        {
            return $this->redirect(['index']);
        }
       
    }
    public function actionViewStatus($id)
    {   
        $request = Yii::$app->request;

        return $this->render('status', [
            'model' => $this->findModel($id),
        ]);

    }
    public function actionViewSprint($id,$sprint)
    {   
        $request = Yii::$app->request;

        return $this->render('chart-sprint', [
            'model' => $this->findModel($id),
            'sprint' => $sprint,
        ]);

    }

    /**
     * Updates an existing Project model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $tip = Yii::$app->user->identity->type;
        if($tip == 1 | $tip == 6 ) 
        {
            $model = $this->findModel($id);       

            if($request->isAjax){
                /*
                *   Process for ajax request
                */
                Yii::$app->response->format = Response::FORMAT_JSON;
                if($request->isGet){
                    return [
                        'title'=> "Изменить Проекта #".$id,
                        'content'=>$this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                    ];         
                }else if($model->load($request->post()) && $model->save()){
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Проект #".$id,
                        'content'=>$this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                    ];    
                }else{
                     return [
                        'title'=> "Изменить Проекта #".$id,
                        'content'=>$this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                    ];        
                }
            }else{
                /*
                *   Process for non-ajax request
                */
                if ($model->load($request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
            }
        }
        else
        {
            if($request->isAjax){            
                Yii::$app->response->format = Response::FORMAT_JSON;                
                if(!$request->isGet){ 
                    return [
                        'title'=> "".'<b>Error</b>',
                        'content'=>'<center><span class="text-error" style="color:red; font-size:20px;"><b>У вас нет прав!!!</b></span></center>',                              
                    ];
                 }                
            }
            else  return $this->redirect(['index']);
        }
    }

    public function actionUpdateProject($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return ['forceClose'=>true,'forceReload'=>'#boards-order-list-ajax'];  
            }else{
                 return [
                    'title'=> "Изменить Проекта #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }
    }

    /**
     * Delete an existing Project model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteProject($id)
    {
        $request = Yii::$app->request;

        /*$sprints = Sprints::find()->where(['communications_project' => $id])->all();
        foreach ($sprints as $sprint) {
            Yii::$app->db->createCommand()->delete('sprints', ['id' => $sprint->id])->execute();
        }
        
        $steps = StagesTasks::find()->where(['communication_id' => $id])->all();
        foreach ($steps as $step) {
            Yii::$app->db->createCommand()->delete('stages_tasks', ['id' => $step->id])->execute();
        }*/

        //$this->findModel($id)->delete();
        Yii::$app->db->createCommand()->update('project', ['is_delete' => 1], [ 'id' =>  $this->findModel($id)->id ])->execute();

        if($request->isAjax){
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#boards-order-list-ajax'];
            //return $this->redirect(['/site/boards-project']);
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['/site/boards-project']);
        }


    }

    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        //$this->findModel($id)->delete();
        Yii::$app->db->createCommand()->update('project', ['is_delete' => 1], [ 'id' =>  $this->findModel($id)->id ])->execute();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Project model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            //$model->delete();
            Yii::$app->db->createCommand()->update('project', ['is_delete' => 1], [ 'id' => $model->id ])->execute();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
