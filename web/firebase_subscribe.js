  var config = {
      apiKey: "AIzaSyDa5H5SmfQdvHLVvSPsK2cvXa4RhM-hcdY",
      authDomain: "control-3a284.firebaseapp.com",
      databaseURL: "https://control-3a284.firebaseio.com",
      projectId: "control-3a284",
      storageBucket: "control-3a284.appspot.com",
    messagingSenderId: "250817074145"
  };
  firebase.initializeApp(config);

  var messaging = firebase.messaging();


  messaging.requestPermission()
  	.then(function() {
  		console.log('Have permission');
  		return messaging.getToken();
  	})
  	.then(function(token){
  		console.log(token);

      if(token){
        console.log('okey');
        sendTokenToServer(token);
      } else {
        setTokenSentToServer(false);
      }
  	})
  	.catch(function(){
  		console.log('Error Ocured');
  	});

 messaging.onMessage(function(payload){
 	console.log(payload);
  var html = _.template($('#alert-template').html())(payload.notification);
  $('#content').prepend(html);
 });


 function sendTokenToServer(currentToken){
    if(!isTokenSentToServer(currentToken)) {
      console.log('Регистрация токена');

      var url = 'https://zvonki.teo-stroy.ru/site/save-token';
      var csrfToken = $('meta[name="csrf-token"]').attr('content');

      $.post(url, {
          '_csrf': csrfToken,
          'token': currentToken
      });

      setTokenSentToServer(currentToken);
    } else {
      console.log('Токен уже зарегистрирован');
    } 
 }

 // используем localStorage для отметки того,
 // что пользователь уже подписался на уведомления
 function isTokenSentToServer(currrentToken){
    return window.localStorage.getItem('sentFirebaseMessagingToken') == currrentToken;
 }

 function setTokenSentToServer(currrentToken){
    window.localStorage.setItem(
        'sentFirebaseMessagingToken',
        currrentToken ? currrentToken : ''
      );
 } 