<?php

use yii\db\Migration;

/**
 * Handles adding status to table `sprint`.
 */
class m171210_060516_add_status_column_to_sprint_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('sprints', 'status', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('sprints', 'stqatus');
    }
}
