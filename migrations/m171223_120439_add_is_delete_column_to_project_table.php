<?php

use yii\db\Migration;

/**
 * Handles adding is_delete to table `project`.
 */
class m171223_120439_add_is_delete_column_to_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('project', 'is_delete', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('project', 'is_delete');
    }
}
