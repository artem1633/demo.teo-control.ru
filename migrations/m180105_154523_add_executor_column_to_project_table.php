<?php

use yii\db\Migration;

/**
 * Handles adding executor to table `project`.
 */
class m180105_154523_add_executor_column_to_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('project', 'executor', $this->integer());

        $this->createIndex('idx-project-executor', 'project', 'executor', false);
        $this->addForeignKey("fk-project-executor", "project", "executor", "users", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-project-executor','project');
        $this->dropIndex('idx-project-executor','project');
        
        $this->dropColumn('project', 'executor');
    }
}
