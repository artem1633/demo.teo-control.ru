<?php

use app\models\Users;
use yii\db\Migration;

/**
 * Class m180501_095644_add_is_super_column_to_companies_column
 */
class m180501_095644_add_is_super_column_to_companies_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('companies', 'is_super', $this->boolean()->defaultValue(0)->comment('Супер компания'));

        $this->insert('companies', [
            'admin_id' => 1,
            'rate_id' => null,
            'is_super' => 1,
        ]);

        $admin = Users::findOne(1);
        $company = \app\models\Companies::findOne(['is_super' => 1]);
        $admin->company_id = $company->id;
        $admin->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('companies', 'is_super');
    }
}
