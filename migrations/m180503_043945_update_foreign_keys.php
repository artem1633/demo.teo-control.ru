<?php

use yii\db\Migration;

/**
 * Class m180503_043945_update_foreign_keys
 */
class m180503_043945_update_foreign_keys extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->setForeignKeyOnDeleteCascade('fk-stages_tasks-communication_id', 'stages_tasks', 'project');
        $this->setForeignKeyOnDeleteCascade('fk-tasks-priority', 'tasks', 'priority');
        $this->setForeignKeyOnDeleteCascade('fk-sprints-communications_project', 'sprints', 'project');
        $this->setForeignKeyOnDeleteCascade('fk-tasks-project', 'tasks', 'project');
        $this->setForeignKeyOnDeleteCascade('fk-tasks-sprint', 'tasks', 'sprints');
        $this->setForeignKeyOnDeleteCascade('fk-tasks-stage', 'tasks', 'stages_tasks');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    }

    private function setForeignKeyOnDeleteCascade($foreignKey, $table, $refTable, $onDelete = 'CASCADE')
    {
        $this->dropForeignKey($foreignKey, $table);

        $column = explode('-', $foreignKey)[2];

        $this->addForeignKey(
            "fk-{$table}-{$column}",
            $table,
            $column,
            $refTable,
            'id',
            $onDelete
        );

    }
}
