<?php

use app\models\Users;
use yii\db\Migration;

/**
 * Class m180501_133728_change_role_first_user
 */
class m180501_133728_change_role_first_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $admin = Users::findOne(1);
        $admin->type = Users::USER_TYPE_SUPER_ADMIN;
        $admin->save();
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }
}
