<?php

use yii\db\Migration;

/**
 * Class m171211_061440_changing_column_to_tasks_table
 */
class m171211_061440_changing_column_to_tasks_table extends Migration
{
    /**
     * @inheritdoc
     */

    public function up()
    {
        $this->alterColumn('tasks', 'time_fact', $this->integer());
        $this->alterColumn('tasks', 'time_scheduled', $this->integer());
    }

    public function down() 
    {
        $this->alterColumn('tasks','time_fact', $this->date());
        $this->alterColumn('tasks','time_scheduled', $this->date());
    } 

}
