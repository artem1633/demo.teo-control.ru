<?php

use yii\db\Migration;

/**
 * Class m171226_115125_changing_values_users_table
 */
class m171226_115125_changing_values_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->update('users', array(
            'creator' => '1',
            'company' => '1',), 
            'id=1'
        );
    }

    public function down()
    {
        
    }
    
}
