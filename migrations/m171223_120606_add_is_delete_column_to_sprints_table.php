<?php

use yii\db\Migration;

/**
 * Handles adding is_delete to table `sprint`.
 */
class m171223_120606_add_is_delete_column_to_sprints_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('sprints', 'is_delete', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('sprints', 'is_delete');
    }
}
