<?php

use yii\db\Migration;

/**
 * Handles adding sorting to table `project`.
 */
class m180618_183920_add_sorting_column_to_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('project', 'sorting', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('project', 'sorting');
    }
}
