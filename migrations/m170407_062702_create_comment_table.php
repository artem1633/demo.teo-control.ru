<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comment`.
 */
class m170407_062702_create_comment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%comment}}', [
            'id' => $this->primaryKey(),
            'entity' => $this->string(),
            'text' => $this->text(),
            'deleted' => $this->boolean()->notNull()->defaultValue(false),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx_entity', '{{%comment}}', ['entity']);
        $this->createIndex('idx_created_by', '{{%comment}}', ['created_by']);
        $this->createIndex('idx_created_at', '{{%comment}}', ['created_at']);

        $this->addColumn('{{%comment}}', 'from', $this->string()->after('entity'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%comment}}', 'from');
        $this->dropTable('{{%comment}}');
    }
}
