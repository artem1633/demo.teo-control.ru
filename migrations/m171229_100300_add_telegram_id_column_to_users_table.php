<?php

use yii\db\Migration;

/**
 * Handles adding telegram_id to table `users`.
 */
class m171229_100300_add_telegram_id_column_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'telegram_id', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'telegram_id');
    }
}
