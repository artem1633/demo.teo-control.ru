<?php

use yii\db\Migration;

/**
 * Handles the creation of table `signup`.
 */
class m171214_093907_create_signup_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('signup', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'type' => $this->string(255),
            'password' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('signup');
    }
}
