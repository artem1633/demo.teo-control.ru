<?php

use yii\db\Migration;

/**
 * Handles adding status to table `stages_tasks`.
 */
class m171218_111450_add_status_column_to_stages_tasks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('stages_tasks', 'status', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('stages_tasks', 'status');
    }
}
