<?php

use yii\db\Migration;

/**
 * Class m171214_183711_add_company_column_to_all_tables
 */
class m171214_183711_add_company_column_to_all_tables extends Migration
{
    public function up()
    {
        $this->addColumn('users', 'company', $this->integer());
        $this->addColumn('technologies', 'company', $this->integer());
        $this->addColumn('board', 'company', $this->integer());
        $this->addColumn('priority', 'company', $this->integer());
        $this->addColumn('type', 'company', $this->integer());
        $this->addColumn('stages_tasks', 'company', $this->integer());
        $this->addColumn('sprints', 'company', $this->integer());
        $this->addColumn('project', 'company', $this->integer());
        $this->addColumn('tasks', 'company', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('users', 'company');
        $this->dropColumn('technologies', 'company');
        $this->dropColumn('board', 'company');
        $this->dropColumn('priority', 'company');
        $this->dropColumn('type', 'company');
        $this->dropColumn('stages_tasks', 'company');
        $this->dropColumn('sprints', 'company');
        $this->dropColumn('project', 'company');
        $this->dropColumn('tasks', 'company');
    }
}
