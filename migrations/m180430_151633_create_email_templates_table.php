<?php

use yii\db\Migration;

/**
 * Handles the creation of table `email_templates`.
 */
class m180430_151633_create_email_templates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('email_templates', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->comment('Ключ'),
            'key_ru' => $this->string()->comment('Ключ на русском'),
            'body' => $this->text()->comment('Тело шаблона'),
            'deletable' => $this->boolean()->defaultValue(true)->comment('Удаляема сущность или нет'),
        ]);
        $this->addCommentOnTable('email_templates', 'Шаблоны для email уведомлений');

        $loginAddress = 'https://demo.teo-control.ru/site/login';

        $this->insert(
            'email_templates',
            [
                'key' => 'success_registration',
                'key_ru' => 'Успешная регистрация',
                'body' => 'Поздравляем вас с успешной регистрацией.<br>Можете авторизоваться по этой ссылке: <a href="'.$loginAddress.'">'.$loginAddress.'</a>',
                'deletable' => false,
            ]
        );

        $this->insert('email_templates', [
            'key' => 'licence_expire',
            'key_ru' => 'Истечение лицензии',
            'body' => 'Уважаемая, компания «{admin.fio}». Ваш срок лицензии истек',
            'deletable' => 0,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('email_templates');
    }
}
