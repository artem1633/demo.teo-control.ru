<?php

use yii\db\Migration;

/**
 * Handles dropping otvetstenniy from table `sprint`.
 */
class m171210_054923_drop_otvetstenniy_column_from_sprint_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropForeignKey('fk-sprints-responsible','sprints');
        $this->dropIndex('idx-sprints-responsible','sprints');
        $this->dropColumn('sprints', 'responsible');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
    }
}
