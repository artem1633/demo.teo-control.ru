<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sprints`.
 */
class m171127_070718_create_sprints_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('sprints', [
            'id' => $this->primaryKey(),
            'communications_project' => $this->integer(),
            'responsible' => $this->integer(),
            'creator' => $this->integer(),
            'date_completion' => $this->date(),
            'date_start' => $this->date(),
            'name' => $this->string(255)->notNull(),
        ]);
         
        $this->createIndex('idx-sprints-communications_project', 'sprints', 'communications_project', false);

        $this->createIndex('idx-sprints-responsible', 'sprints', 'responsible', false);
       

        $this->createIndex('idx-sprints-creator', 'sprints', 'creator', false);
        
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        $this->dropIndex('idx-sprints-communications_project','sprints');

        $this->dropForeignKey('fk-sprints-responsible','sprints');
        

        $this->dropForeignKey('fk-sprints-creator','sprints');
       

        $this->dropTable('sprints');
    }
}
