<?php

use yii\db\Migration;

/**
 * Handles adding start_data to table `tasks`.
 */
class m180110_102720_add_start_data_column_to_tasks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('tasks', 'start_date', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('tasks', 'start_date');
    }
}
