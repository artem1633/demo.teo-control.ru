<?php

use yii\db\Migration;

/**
 * Class m180503_125632_update_foreign_keys
 */
class m180503_125632_update_foreign_keys extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->setForeignKeyOnDeleteCascade('fk-tasks-project', 'tasks', 'project', 'SET NULL');
        $this->setForeignKeyOnDeleteCascade('fk-tasks-sprint', 'tasks', 'sprints', 'SET NULL');
        $this->setForeignKeyOnDeleteCascade('fk-tasks-stage', 'tasks', 'stages_tasks', 'SET NULL');
        $this->setForeignKeyOnDeleteCascade('fk-tasks-priority', 'tasks', 'priority', 'SET NULL');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }

    private function setForeignKeyOnDeleteCascade($foreignKey, $table, $refTable, $onDelete = 'CASCADE')
    {
        $this->dropForeignKey($foreignKey, $table);

        $column = explode('-', $foreignKey)[2];

        $this->addForeignKey(
            "fk-{$table}-{$column}",
            $table,
            $column,
            $refTable,
            'id',
            $onDelete
        );

    }
}
