<?php

use yii\db\Migration;

/**
 * Class m180430_135225_delete_old_companies_system_filds_from_all_tables
 */
class m180430_135225_delete_old_companies_system_filds_from_all_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->safeDropColumn('board', 'company');
        $this->safeDropColumn('priority', 'company');
        $this->safeDropColumn('project', 'company');
        $this->safeDropColumn('sprints', 'company');
        $this->safeDropColumn('stages_tasks', 'company');
        $this->safeDropColumn('users', 'company');
        $this->safeDropColumn('type', 'company');
        $this->safeDropColumn('technologies', 'company');
        $this->safeDropColumn('tasks', 'company');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }

    /**
     * @param string $table
     * @param string $column
     */
    private function safeDropColumn($table, $column)
    {
        if(isset(Yii::$app->db->schema->getTableSchema($table)->columns[$column]))
        {
            $this->dropColumn($table, $column);
        }
    }
}
