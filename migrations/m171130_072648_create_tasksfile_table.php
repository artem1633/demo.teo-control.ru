<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tasksfile`.
 */
class m171130_072648_create_tasksfile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tasksfile', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'title' => $this->string(25),
            'path' => $this->text(),
        ]);

        $this->createIndex('idx-tasksfile-order_id', 'tasksfile', 'order_id', false);
        $this->addForeignKey("fk-tasksfile-order_id", "tasksfile", "order_id", "tasks", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-tasksfile-order_id','tasksfile');
        $this->dropIndex('idx-tasksfile-order_id','tasksfile');

        $this->dropTable('tasksfile');
    }
}
