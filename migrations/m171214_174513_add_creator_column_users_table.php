<?php

use yii\db\Migration;

/**
 * Class m171214_174513_add_creator_column_users_table
 */
class m171214_174513_add_creator_column_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'creator', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('users', 'creator');
    }
}
