<?php

use yii\db\Migration;

/**
 * Class m180430_134810_add_last_datetime_activity_to_users_and_companies_tables
 */
class m180430_134810_add_last_datetime_activity_to_users_and_companies_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'last_activity_datetime', $this->dateTime()->comment('Дата и время последней активности'));
        $this->addColumn('companies', 'last_activity_datetime', $this->dateTime()->comment('Дата и время последней активности'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'last_activity_datetime');
        $this->dropColumn('companies', 'last_activity_datetime');
    }
}
