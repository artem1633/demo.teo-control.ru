<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m171127_071046_create_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'creator_composer' => $this->integer(),
            'responsible' => $this->integer(),
            'status' => $this->string(255),
        ]);

        $this->addForeignKey("fk-sprints-communications_project", "sprints", "communications_project", "project", "id");
        $this->addForeignKey("fk-stages_tasks-communication_id", "stages_tasks", "communication_id", "project", "id");

        $this->createIndex('idx-project-creator_composer', 'project', 'creator_composer', false);
       

        $this->createIndex('idx-project-responsible', 'project', 'responsible', false);
       
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-sprints-communications_project','sprints');
        $this->dropIndex('idx-stages_tasks-communication_id','stages_tasks');

         $this->dropForeignKey('fk-project-creator_composer','project');
       

        $this->dropForeignKey('fk-project-responsible','project');
       

        $this->dropTable('project');
    }
}
