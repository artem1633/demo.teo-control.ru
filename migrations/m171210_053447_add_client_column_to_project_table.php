<?php

use yii\db\Migration;

/**
 * Handles adding client to table `project`.
 */
class m171210_053447_add_client_column_to_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('project', 'client', $this->integer());

        $this->createIndex('idx-project-client', 'project', 'client', false);
        $this->addForeignKey("fk-project-client", "project", "client", "users", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('project', 'client');

        $this->dropForeignKey('fk-project-client','project');
        $this->dropIndex('idx-project-client','project');
    }
}
