<?php

use yii\db\Migration;

/**
 * Handles adding new to table `rates`.
 */
class m180430_134708_add_new_columns_to_rates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rates', 'price', $this->float()->comment('Стоимость тарифа'));
        $this->addColumn('rates', 'time', $this->float()->comment('Период действия лицензии с момента ее получения (в секундах)'));
        $this->addColumn('rates', 'sort', $this->float()->comment('Сортировка'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('rates', 'price');
        $this->dropColumn('rates', 'time');
        $this->dropColumn('rates', 'sort');
    }
}
