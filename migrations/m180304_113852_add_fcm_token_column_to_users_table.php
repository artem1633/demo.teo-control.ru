<?php

use yii\db\Migration;

/**
 * Handles adding fcm_token to table `users`.
 */
class m180304_113852_add_fcm_token_column_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'fcm_token', $this->string()->comment('FCM токен для получения пуш уведомлений'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'fcm_token');
    }
}
