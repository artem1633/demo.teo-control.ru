<?php

use yii\db\Migration;

/**
 * Class m180430_134449_add_rate_id_to_company_table
 */
class m180430_134449_add_rate_id_to_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('companies', 'rate_id', $this->integer()->comment('Тариф'));

        $this->createIndex(
            'idx-companies-rate_id',
            'companies',
            'rate_id'
        );

        $this->addForeignKey(
            'fk-companies-rate_id',
            'companies',
            'rate_id',
            'rates',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-companies-rate_id',
            'companies'
        );

        $this->dropIndex(
            'idx-companies-rate_id',
            'companies'
        );

        $this->dropColumn('companies', 'rate_id');
    }
}
