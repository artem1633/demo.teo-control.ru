<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tasks`.
 */
class m171127_094526_create_tasks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tasks', [
            'id' => $this->primaryKey(),
            'author' => $this->integer()->comment('aвтор'),
            'manager_project' => $this->integer()->comment('менеджер проекта'),
            'executor' => $this->integer()->comment('исполнитель'),
            'priority' => $this->integer()->comment('приоритет'),
            'project' => $this->integer()->comment('проект'),
            'stage' => $this->integer()->comment('этап'),
            'sprint' => $this->integer()->comment('спринт'),
            'types_directory' => $this->integer()->comment('cправочник типов'),
            'commentary' => $this->text()->comment('комментарии'),
            'file' => $this->string(255)->comment('файл'),
            'description' => $this->text()->comment('описание'),
            'date_delivery' => $this->date()->comment('дата сдачи'),
            'time_fact' => $this->date()->comment('дата по факту'),
            'time_scheduled' => $this->date()->comment('время запланировано'),
            'task_title' => $this->string(255)->comment('название задачи'),
        ]);
        $this->createIndex('idx-tasks-author', 'tasks', 'author', false);
        

        $this->createIndex('idx-tasks-manager_project', 'tasks', 'manager_project', false);
        

        $this->createIndex('idx-tasks-executor', 'tasks', 'executor', false);
       

        $this->createIndex('idx-tasks-priority', 'tasks', 'priority', false);
        $this->addForeignKey("fk-tasks-priority", "tasks", "priority", "priority", "id");

        $this->createIndex('idx-tasks-types_directory', 'tasks', 'types_directory', false);
        $this->addForeignKey("fk-tasks-types_directory", "tasks", "types_directory", "type", "id");

        $this->createIndex('idx-tasks-stage', 'tasks', 'stage', false);
        $this->addForeignKey("fk-tasks-stage", "tasks", "stage", "stages_tasks", "id");

        $this->createIndex('idx-tasks-sprint', 'tasks', 'sprint', false);
        $this->addForeignKey("fk-tasks-sprint", "tasks", "sprint", "sprints", "id");

        $this->createIndex('idx-tasks-project', 'tasks', 'project', false);
        $this->addForeignKey("fk-tasks-project", "tasks", "project", "project", "id");


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-tasks-author','tasks');
        

        $this->dropForeignKey('fk-tasks-manager_project','tasks');
        

        $this->dropForeignKey('fk-tasks-executor','tasks');
        

        $this->dropForeignKey('fk-tasks-priority','tasks');
        $this->dropIndex('idx-tasks-priority','tasks');

        $this->dropForeignKey('fk-tasks-types_directory','tasks');
        $this->dropIndex('idx-tasks-types_directory','tasks');

        $this->dropForeignKey('fk-tasks-stage','tasks');
        $this->dropIndex('idx-tasks-stage','tasks');

        $this->dropForeignKey('fk-tasks-sprint','tasks');
        $this->dropIndex('idx-tasks-sprint','tasks');

        $this->dropForeignKey('fk-tasks-project','tasks');
        $this->dropIndex('idx-tasks-project','tasks');


        $this->dropTable('tasks');
    }
}
