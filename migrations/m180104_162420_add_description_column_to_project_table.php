<?php

use yii\db\Migration;

/**
 * Handles adding description to table `project`.
 */
class m180104_162420_add_description_column_to_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('project', 'description', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('project', 'description');
    }
}
