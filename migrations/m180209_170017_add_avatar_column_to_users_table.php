<?php

use yii\db\Migration;

/**
 * Handles adding avatar to table `users`.
 */
class m180209_170017_add_avatar_column_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'avatar', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'avatar');
    }
}
