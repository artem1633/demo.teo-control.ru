<?php

use yii\db\Migration;

/**
 * Handles the creation of table `board_project`.
 */
class m171201_075923_create_board_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('board_project', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('board_project');
    }
}
