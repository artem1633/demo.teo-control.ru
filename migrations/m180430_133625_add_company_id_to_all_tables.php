<?php

use yii\db\Migration;

/**
 * Class m180430_133625_add_company_id_to_all_tables
 */
class m180430_133625_add_company_id_to_all_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addCompanyIdField('board');
        $this->addCompanyIdField('board_project');
        $this->addCompanyIdField('changing');
        $this->addCompanyIdField('comment');
        $this->addCompanyIdField('icons');
        $this->addCompanyIdField('priority');
        $this->addCompanyIdField('project');
        $this->addCompanyIdField('sprints');
        $this->addCompanyIdField('stages_tasks');
        $this->addCompanyIdField('tasks');
        $this->addCompanyIdField('tasksfile');
        $this->addCompanyIdField('technologies');
        $this->addCompanyIdField('type');
        $this->addCompanyIdField('users');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropCompanyIdField('board');
        $this->dropCompanyIdField('board_project');
        $this->dropCompanyIdField('changing');
        $this->dropCompanyIdField('comment');
        $this->dropCompanyIdField('icons');
        $this->dropCompanyIdField('priority');
        $this->dropCompanyIdField('project');
        $this->dropCompanyIdField('sprints');
        $this->dropCompanyIdField('stages_tasks');
        $this->dropCompanyIdField('tasks');
        $this->dropCompanyIdField('tasksfile');
        $this->dropCompanyIdField('technologies');
        $this->dropCompanyIdField('type');
        $this->dropCompanyIdField('users');
    }


    /**
     * Добавляет поле company_id
     * @param $tableName
     */
    private function addCompanyIdField($tableName)
    {
        $this->addColumn($tableName, 'company_id', $this->integer()->comment('Компания, к которой принадлежит запись'));

        $this->createIndex(
            "idx-{$tableName}-company_id",
            $tableName,
            "company_id"
        );

        $this->addForeignKey(
            "fk-{$tableName}-company_id",
            $tableName,
            "company_id",
            "companies",
            "id",
            "CASCADE"
        );
    }

    /**
     * Удалить поле company_id
     * @param $tableName
     */
    private function dropCompanyIdField($tableName)
    {
        $this->dropForeignKey(
            "fk-{$tableName}-company_id",
            $tableName
        );

        $this->dropIndex(
            "idx-{$tableName}-company_id",
            $tableName
        );

        $this->dropColumn($tableName, 'company_id');
    }
}
