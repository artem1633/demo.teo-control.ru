<?php

use yii\db\Migration;

/**
 * Handles adding vk_href to table `users`.
 */
class m180430_173521_add_vk_href_column_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users','vk_href', $this->string()->comment('Ссылка на страницу ВКонтакте'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('users', 'vk_id');
    }
}
