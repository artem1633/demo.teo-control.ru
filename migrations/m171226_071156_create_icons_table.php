<?php

use yii\db\Migration;

/**
 * Handles the creation of table `icons`.
 */
class m171226_071156_create_icons_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('icons', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
        ]);

        $this->insert('icons',array(
            'title' => 'glyphicon glyphicon-ok',
        ));

        $this->insert('icons',array(
            'title' => 'glyphicon glyphicon-plus',
        ));

        $this->insert('icons',array(
            'title' => 'glyphicon glyphicon-minus',
        ));

        $this->insert('icons',array(
            'title' => 'glyphicon glyphicon-pencil',
        ));

        $this->insert('icons',array(
            'title' => 'glyphicon glyphicon-remove',
        ));

        $this->insert('icons',array(
            'title' => 'glyphicon glyphicon-trash',
        ));

        $this->insert('icons',array(
            'title' => 'glyphicon glyphicon-time',
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('icons');
    }
}
