<?php

use yii\db\Migration;

/**
 * Handles the creation of table `stages_tasks`.
 */
class m171127_065141_create_stages_tasks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('stages_tasks', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'sorting' => $this->integer(),
            'color' => $this->string(255)->notNull(),
            'communication_id' => $this->integer(),
            'show' => $this->string(20),
        ]);

        $this->createIndex('idx-stages_tasks-communication_id', 'stages_tasks', 'communication_id', false);
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-stages_tasks-communication_id','stages_tasks');
        
        
        $this->dropTable('stages_tasks');
    }
}
