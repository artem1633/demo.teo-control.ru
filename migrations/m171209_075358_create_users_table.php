<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m171209_075358_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255)->notNull(),
            'login' => $this->string(255)->notNull(),
            'password' => $this->string(255)->notNull(),
            'telephone' => $this->string(255),
            'city' => $this->string(255),
            'type' => $this->integer()->notNull(),
            'dostup' => $this->boolean(),
            'comment' => $this->text(),
            'technology' => $this->string(255),
            'cost' => $this->integer(),
            'otsenka_opit' => $this->integer(),
            'opit_god' => $this->integer(),

        ]);

        $this->insert('users',array(
            'fio' => 'Иванов Иван Иванович',
            'login' => 'ivanov@gmail.com',
            'password' => md5('admin'),
            'telephone' => '',
            'city' => 'Ташкент',
            'type' => 0,
            'dostup' => 1,
            'comment' => '',
            'technology' => 'Yii2',
            'cost' => 500,
            'otsenka_opit' => 5,
            'opit_god' => 2,
        ));

         $this->addForeignKey("fk-project-creator_composer", "project", "creator_composer", "users", "id");

          $this->addForeignKey("fk-project-responsible", "project", "responsible", "users", "id");

           $this->addForeignKey("fk-sprints-responsible", "sprints", "responsible", "users", "id");

           $this->addForeignKey("fk-sprints-creator", "sprints", "creator", "users", "id");

           $this->addForeignKey("fk-tasks-author", "tasks", "author", "users", "id");

           $this->addForeignKey("fk-tasks-manager_project", "tasks", "manager_project", "users", "id");
 $this->addForeignKey("fk-tasks-executor", "tasks", "executor", "users", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
         $this->dropIndex('idx-project-creator_composer','project');

          $this->dropIndex('idx-project-responsible','project');

          $this->dropIndex('idx-sprints-responsible','sprints');

           $this->dropIndex('idx-sprints-creator','sprints');

           $this->dropIndex('idx-tasks-author','tasks');
$this->dropIndex('idx-tasks-manager_project','tasks');
$this->dropIndex('idx-tasks-executor','tasks');

        $this->dropTable('users');
    }
}
