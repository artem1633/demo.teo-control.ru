<?php

namespace app\components;

use yii\base\Component;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use app\models\Settings;

class VK extends Component
{
    /**
     * @var string Токен для аутентификации в VK
     */
    public $access_token;

    /**
     * @var string Обязательный параметр для отправки запроса
     */
    public $url = 'https://api.vk.com/method/messages.send';

    /**
     * @var string Обязательный параметр для отправки запроса
     */
    public $v = '5.37';

    /**
     * @var string id получателя
     */
    private $to;

    /**
     * @var string сообщение
     */
    private $message;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $setting = Settings::findByKey('vk_access_token');

        if($setting != null){
            $this->access_token = $setting->value;
        }

        if(!$this->checkRequiredParams()){
            return null;
        }

        parent::init();
    }

    /**
     * @param string $to
     * @return $this
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @param $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return bool|string
     * @throws Exception
     */
    public function send()
    {
        $url = $this->url;

        if(!$this->checkRequiredParams()){
            return null;
        };

        if($this->to === null){
            return null;
        }

        if($this->message === null){
            return null;
        }

        $params = array(
            'user_id' => $this->to,    // Кому отправляем
            'message' => $this->message,   // Что отправляем
            'access_token' => $this->access_token,  // access_token можно вбить хардкодом, если работа будет идти из под одного юзера
            'v' => $this->v,
        );

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));

        return $result;
    }


    /**
     * @throws InvalidConfigException
     */
    private function checkRequiredParams()
    {
        if($this->access_token === null || $this->url === null || $this->v === null){
//            throw new InvalidConfigException('Параметры $access_token, $url, $v не должны быть NULL');
            return false;
        }
        return true;
    }
}