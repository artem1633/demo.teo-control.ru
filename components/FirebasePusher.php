<?php

namespace app\components;

use yii\base\BaseObject;

/**
 * Class FirebasePusher
 * @package app\components
 */
class FirebasePusher extends BaseObject
{
    /**
     * @var String Ключ от API Firebase cloud messaging, который получают в личном кабинете
     * @see https://console.firebase.google.com/project/
     */
    public $apiKey;

    /**
     * Отправляет токен
     * return String
     */
    public function push()
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $YOUR_TOKEN_ID = 'db_gxTlYVNc:APA91bFfPn6TI1_lMkmdYKQq5EAN1bFbDYvHucAbZzdr_VnoYK4x1EMdhlMoYZt3sHM-YMK0uaXA1AqY6YP0_lcvnbZPDJ4yKuObwzAZ7QvJzYXindt7q0WsnD8JgLFvCAYzf89diDYD';

        $request_body = [
            'to' => $YOUR_TOKEN_ID,
            'notification' => [
                'title' => 'Test',
                'body' => "Test message",
                'icon' => '',
                'click_action' => 'https://google.com/',
            ],
        ];
        $fields = json_encode($request_body);

        $request_headers = [
            'Content-Type: application/json',
            'Authorization: key=' . $this->apiKey,
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        curl_close($ch);

        echo $response;
    }
}