<?php

namespace app\event_handlers;

use app\models\Logs;
use app\models\RegisterForm;
use yii\base\Behavior;

/**
 * Class RegisteredFormEventHandler
 * @package app\event_handlers
 * @see \app\models\RegisterForm
 *
 * Обработчик событий класса RegisterForm
 */
class RegisteredFormEventHandler extends Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            RegisterForm::EVENT_REGISTERED => 'registered',
        ];
    }

    /**
     * @param $event
     */
    public function registered($event)
    {
        $user = $event->sender->_user;

        $log = new Logs([
            'user_id' => $user->id,
            'event_datetime' => date('Y-m-d H:i:s'),
            'event' => Logs::EVENT_USER_REGISTERED,
        ]);

        $log->description = $log->generateEventDescription();
        $log->save();
    }
}