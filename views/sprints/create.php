<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Sprints */

?>
<div class="sprints-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
