<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
use app\models\Users;
use app\models\Project;

/* @var $this yii\web\View */
/* @var $model app\models\Sprints */
/* @var $form yii\widgets\ActiveForm */
$user = Users::findOne(Yii::$app->user->identity->id);

$tip = Yii::$app->user->identity->type;
if($tip == 3) $project = Project::find()->where(['responsible' => Yii::$app->user->identity->id])->all();
else $project = Project::find()->all();
$pr = ArrayHelper::map($project,'id','name');

?>

<div class="sprints-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'communications_project')->dropDownList($pr, ['prompt'=>'Выберите ']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'date_start')->widget(
                DatePicker::className(), [
                    'inline' => false,
                    'language' => 'ru',
                    'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'date_completion')->widget(
                DatePicker::className(), [
                    'inline' => false,
                    'language' => 'ru',
                    'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList($model->getStatus(), ['prompt'=>'Выберите статуса']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'is_delete')->dropDownList($model->getDeleted(), ['prompt'=>'Выберите']) ?>
        </div>
    </div>

    <?php // $form->field($model, 'company_id')->textInput() ?>

    <div style="display: none;">
        <?= $form->field($model, 'creator')->textInput(['value'=>Yii::$app->user->identity->id]) ?>
    </div>
  
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
