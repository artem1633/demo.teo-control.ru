<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sprints */
?>
<div class="sprints-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'communications_project',
            'creator',
            'date_completion',
            'date_start',
            'name',
            'status',
            //'company',
        ],
    ]) ?>

</div>
