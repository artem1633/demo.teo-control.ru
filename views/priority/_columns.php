<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'icon',
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'icon',
        'content' => function ($data) {
            /*if($data->icon == 1) { $r = $data->getIconname(1); return '<span class="'.$r.'"></span>  ' . $data->getIconname(1);}
            if($data->icon == 2) { $r = $data->getIconname(2); return '<span class="'.$r.'"></span>  ' . $data->getIconname(2);}
            if($data->icon == 3) { $r = $data->getIconname(3); return '<span class="'.$r.'"></span>  ' . $data->getIconname(3);}
            if($data->icon == 4) { $r = $data->getIconname(4); return '<span class="'.$r.'"></span>  ' . $data->getIconname(4);}
            if($data->icon == 5) { $r = $data->getIconname(5); return '<span class="'.$r.'"></span>  ' . $data->getIconname(5);}
            if($data->icon == 6) { $r = $data->getIconname(6); return '<span class="'.$r.'"></span>  ' . $data->getIconname(6);}
            if($data->icon == 7) { $r = $data->getIconname(7); return '<span class="'.$r.'"></span>  ' . $data->getIconname(7);}*/
            $icon = $data->getIconname(); //\app\models\Icons::findOne($data->icon);
            return '<span class="'.$icon.'"></span>  ' . $icon;
        },
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company',
    ],*/
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   