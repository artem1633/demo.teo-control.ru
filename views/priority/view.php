<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Priority */
?>
<div class="priority-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'icon',
            //'company',
        ],
    ]) ?>

</div>
