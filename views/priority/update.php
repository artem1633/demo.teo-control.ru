<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Priority */
?>
<div class="priority-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
