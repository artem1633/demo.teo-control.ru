<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StagesTasks */
?>
<div class="stages-tasks-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
