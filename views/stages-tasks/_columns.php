<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'content'=>function($data){
            if ($data->status == 1){ return 'Планируемая'; }
            if ($data->status == 2){ return 'В работе'; }
            if ($data->status == 3){ return 'Выполненой'; }
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sorting',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'color',
        'content'=>function($data){
            if ($data->color == 1){ return 'Красный'; }
            if ($data->color == 2){ return 'Зелёный'; }
            if ($data->color == 3){ return 'Синий'; }
            if ($data->color == 4){ return 'Жёлтый'; }
            if ($data->color == 5){ return 'Чёрный'; }
            if ($data->color == 6){ return 'Белый'; }
            if ($data->color == 7){ return 'Коричневый'; }
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'communication_id',
        'value' => 'communication.name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'show',
        'content'=>function($data){
            if ($data->show == 1){ return 'Да'; }
            if ($data->show == 2){ return 'Нет'; }
        },
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'company',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   