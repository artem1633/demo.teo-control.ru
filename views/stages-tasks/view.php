<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StagesTasks */
?>
<div class="stages-tasks-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'sorting',
            'color',
            'communication_id',
            'show',
            //'company',
        ],
    ]) ?>

</div>
