<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StagesTasks */

?>
<div class="stages-tasks-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
