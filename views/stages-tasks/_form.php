<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Project;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $model app\models\StagesTasks */
/* @var $form yii\widgets\ActiveForm */

/*$user = Users::findOne(Yii::$app->user->identity->id);
if($user->creator != null) $company = $user->creator;
else $company = Yii::$app->user->identity->id;*/

$project = Project::find()/*->where(['company' => $company])*/->all(); 
$pr = ArrayHelper::map($project,'id','name');

?>

<div class="stages-tasks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList($model->getStatus()) ?>

    <?= $form->field($model, 'sorting')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'color')->dropDownList($model->getColor(), ['prompt'=>'Выберите цвета']) ?>
    
    <?= $form->field($model, 'communication_id')->dropDownList($pr, ['prompt'=>'Выберите ']) ?>
    
    <?= $form->field($model, 'show')->dropDownList($model->getShow(), ['prompt'=>'Выберите']) ?>

    <?php // $form->field($model, 'company')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
