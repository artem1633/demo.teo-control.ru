<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Board */
/* @var $form yii\widgets\ActiveForm */

/* $manager_project = \app\models\User::find()->all(); 
 $meneger_project=ArrayHelper::map($manager_project,'id','username');*/
?>

<div class="row">
    <div class="steps-form">

        <?php $form = ActiveForm::begin([
            'id' => 'step-sprint-form',
            //'action' => $action,
        ]); ?>

        <div style="display: none;">
            <?= $form->field($model, 'communication_id')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'sorting')->textInput(['type' => 'number']) ?>

        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'color')->dropDownList($model->getColor()) ?>
            <?= $form->field($model, 'show')->dropDownList($model->getShow()) ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>


        <?php ActiveForm::end(); ?>
        <div id="error-msg"></div>
    </div>
</div>