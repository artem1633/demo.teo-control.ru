<?php
use yii\helpers\Url;
use app\models\Tasks;
use yii\helpers\Html;
use yii\widgets\Pjax;
?>

<?php foreach ($boards as $item):?>

    <?php $ready = 0; $all = 0;
        $tasks = Tasks::find()->where(['project' => $item->id])->all();
        foreach ($tasks as $task) {
            if($task->stage0->status == 3) $ready++;
            $all++;
        }
        if($all == 0)$protsent = 0;
        else $protsent = 100*($ready/$all);
        $int_protsent = (int)$protsent;
        ?>

    <div class="col-md-3">
                            <div class="panel panel-primary">
			            <div class="panel-heading" style="background-color: #005bbf;">
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'second-pjax']) ?>
                            <div class="panel-heading-btn">
                                <?= Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', ['/project/update-project', 'id' => $item->id], [
                                    'role'=>'modal-remote', 'title'=>'Изменить',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                ]) ?>
                                <?= Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', ['/project/delete-project','id'=>$item->id], [
                                    'role'=>'modal-remote', 'title'=>'Удалить',
                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                    'data-request-method'=>'post',
                                    'data-confirm-title'=>'Вы уверены?',
                                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                                ]) ?>
                            </div>
                            <?php Pjax::end() ?>
                            <div style="width: 100px; margin-left: 8px;" class="progress progress-striped progress-sm active pull-right m-t-5"><div class="progress-bar progress-bar-success" style="width: <?=$int_protsent?>%; background-color: #4CD964;"><?php echo $int_protsent; ?>%</div></div>
                            <h4 class="panel-title">Проект</h4>
			            </div>
			            <div class="panel-body" style="background-color: #007AFF !important;">
                                <a style="color: #fff;" href="<?=Url::to(['/site/boards-sprint', 'project_id' => $item->id])?>">
                                    <span class="board-tile-details is-badged">
                                        <span title="CRM мини" dir="auto" class="board-tile-details-name"><?=$item->name?></span>
                                    </span>
                                </a>
                        </div>
            </div>
            </div>
<?php endforeach;?>