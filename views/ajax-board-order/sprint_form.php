<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;

?>

<?php $form = ActiveForm::begin(['id' => 'ajax-board-order-sprint-form',]); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'date_start')->widget(
                DatePicker::className(), [
                'inline' => false,
                'language' => 'ru',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'date_completion')->widget(
                DatePicker::className(), [
                'inline' => false,
                'language' => 'ru',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'status')->dropDownList($model->getStatus(), ['prompt'=>'Выберите статуса']) ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>                
    <div style="display: none;">
        <?= $form->field($model, 'communications_project')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'creator')->textInput(['value'=>Yii::$app->user->identity->id]) ?>
    </div>
<?php ActiveForm::end(); ?>