<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Users;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */

/*$user = Users::findOne(Yii::$app->user->identity->id);
if($user->creator != null){
    $responsibles = Users::find()->where(['type' => 3, 'creator' => $user->creator ])->all(); 
}
else {
    $responsibles = Users::find()->where(['type' => 3,'creator' => Yii::$app->user->identity->id])->all();
}*/
$responsibles = Users::find()->where(['type' => [1,3], 'company_id' => Yii::$app->user->identity->company_id])->all();
$responsible=ArrayHelper::map($responsibles,'id','fio');

$progers = Users::find()->where(['type' => [1,5], 'company_id' => Yii::$app->user->identity->company_id])->all();
$proger=ArrayHelper::map($progers,'id','fio');


if($user->creator != null){
    $client = Users::find()->where(['type' => 4, 'creator' => $user->creator])->all(); 
}
else {
    $client = Users::find()->where(['type' => 4,'creator' => Yii::$app->user->identity->id])->all(); 
}
$clients=ArrayHelper::map($client,'id','fio');

?>
<?php $form = ActiveForm::begin([ 'id' => 'ajax-board-order-project-form', ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'responsible')->dropDownList($responsible, ['prompt'=>'Выберите ответственного']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'client')->dropDownList($clients, ['prompt'=>'Выберите клиента']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model->getStatus(), ['prompt'=>'Выберите статуса']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'executor')->dropDownList($proger/*$model->ExecutorList()*/, ['prompt'=>'Выберите исполнителя']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'description')->widget(CKEditor::className(),[
                'editorOptions' => [
                'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                    'height' => '120px',
                ],
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
        <div class="col-md-12" style="display: none;">
            <?= $form->field($model, 'creator_composer')->textInput(['value'=>Yii::$app->user->identity->id]) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>