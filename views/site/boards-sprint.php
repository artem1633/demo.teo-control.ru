<?php
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use app\assets\SprintAsset;
use app\models\Tasks;
use app\models\Project;
use yii\widgets\Pjax;

SprintAsset::register($this);
$tip = Yii::$app->user->identity->type;
$project = Project::findOne($project_id);

$dostup = 0;
if($tip == 1 | $tip == 6) $dostup = 1;
if($tip == 3) $dostup = 1;

$this->title = 'Персональные доски спринтов';
?>

    <div class="boards-page-board-section-header">
        
    </div>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a href="<?= Url::to(['/site/boards-project'])?>"  style="color:red; " >
                <i class="glyphicon glyphicon-backward"></i> <b>К проектам </b>
            </a>
            <center style="margin-top: -20px; color:yellow; font-weight: bold;"> <?=$project->name?> </center>
            <i style="float: right; margin-top: -20px;"> Спринты </i>
        </h4> 
    </div>
    <div class="panel-body">

    <ul class="boards-page-board-section-list" id="" style="margin-left: -40px;">
    <span id="boards-sprint-list-ajax">
        <?php foreach ($boards as $item):?>

            <?php $ready = 0; $all = 0;
                $tasks = Tasks::find()->where(['sprint' => $item->id])->all();
                foreach ($tasks as $task) {
                    if($task->stage0->status == 3) $ready++;
                    $all++;
                }
                if($all == 0)$protsent = 0;
                else $protsent = 100*($ready/$all);
                $int_protsent = (int)$protsent;
                if($item->status == 1) { $status =  "Готово"; $color = "#4cd964"; }//green
                if($item->status == 2) { $status =  "Планируеться"; $color = "#ff9500"; }//yellow
                if($item->status == 3) { $status =  "в Работе"; $color = "#007aff"; }//blue
            ?>

            <div class="col-md-3">
                <div class="panel panel-primary">
            <div class="panel-heading" style="background-color: #005bbf;">
                            <?php Pjax::begin(['enablePushState' => false, 'id' => 'second-pjax']) ?>
                            <div class="panel-heading-btn">

                            </div>
                            <?php Pjax::end() ?>
                            <div style="width: 100px; margin-left: 8px;" class="progress progress-striped progress-sm active pull-right m-t-5"><div class="progress-bar progress-bar-success" style="width: <?=$int_protsent?>%; background-color: #4CD964;"><?php echo $int_protsent; ?>%</div></div>
                            <h4 class="panel-title">Спринт</h4>
                        </div>
        <div class="panel-body" style="background-color: <?=$color?> !important;">
            <li class="boards-page-board-section-list-item">
                <div class="box-header">

                    <a style="color: #fff;" href="<?= Url::to(['/site/board-detail-sprint', 'sprint_id' => $item->id])?>">
                        <span class="board-tile-details1 is-badged">
                            <span title="CRM мини" dir="auto" class="board-tile-details-name"><?=$item->name?></span>
                        </span>
                    </a>
                </div>
                <?php $task_count = Tasks::find()->where(['sprint' => $item->id])->count();
                    $tasks = Tasks::find()->where(['sprint' => $item->id])->all();
                    $worked = 0; $ready = 0; $worked_hour = 0; $ready_hour = 0; $all_hour = 0;
                    foreach ($tasks as $value) {
                      $all_hour += $value->time_fact;
                      if($value->stage0->status == 2) { $worked++; $worked_hour += $value->time_fact;}
                      if($value->stage0->status == 3) { $ready++; $ready_hour += $value->time_fact;}
                    }
                ?>

                <h6 style="color: white;">Всего \В работе \Выполнено </h6>
                <h6 style="color: white;">
                    <span style="margin-left: 4px;"><?= $task_count?></span>
                    <span style="margin-left: 50px;"><?= $worked ?></span> 
                    <span style="margin-left: 50px;"><?= $ready ?></span> 
                </h6>
                <h6 style="color: white;">
                    <span style="margin-left: 4px;"><?= $all_hour?></span>
                    <span style="margin-left: 50px;"><?= $worked_hour ?></span> 
                    <span style="margin-left: 50px;"><?= $ready_hour ?></span> 
                </h6>
            </li>


                <div class="ui-258">
                    <?php if($dostup == 1){ ?>
                        <!-- Icon -->
                        <div class="ui-icon bg-lblue">
                            <!-- Удалить -->
                            <?= Html::a('<i class="fa fa-trash text" style="font-size: 16px;"></i>', ['/sprints/delete-sprint','id'=>$item->id], [
                                'role'=>'modal-remote', 'title'=>'Удалить',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                'data-request-method'=>'post',
                                'data-confirm-title'=>'Вы уверены?',
                                'data-confirm-message'=>'Вы действительно хотите удалить данную запись?',
                                'class'=>'ui-tooltip'
                            ]) ?>
                        </div>
                        <div class="ui-icon bg-lblue">
                            <!-- Изменить -->
                            <?= Html::a('<i class="fa fa-pencil text" style="font-size: 16px;"></i>', ['/sprints/update-sprint', 'id' => $item->id], [
                                'role'=>'modal-remote', 'title'=>'Изменить',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                'data-request-method'=>'post',
                                'class'=>'ui-tooltip'
                            ]) ?>
                        </div>
                    <!-- Icon -->
                        <div class="ui-icon bg-lblue">
                            <!-- Добавить задачу -->
                            <?=Html::a('<i class="fa fa-plus"></i></a>',
                                [   '/tasks/create',
                                    'project' => $item->communications_project,
                                    'sprint' => $item->id,
                                    'executor' => $item->communicationsProject->executor,
                                    'manager' => $item->communicationsProject->responsible,
                                    'url' => 'site/boards-sprint?project_id='.$item->communications_project ],
                                [ 'target' => '_blank', 'title' => 'Добавить задачи', 'class'=> 'ui-tooltip' ])?>

                        </div>
                    <?php } ?>
                        <!-- Icon -->
                        <div class="ui-icon bg-lblue">
                            <!-- Список -->
                            <a href="<?= Url::to(['/site/board-detail-sprint', 'sprint_id' => $item->id])?>"
                               class="ui-tooltip" data-toggle="tooltip" data-placement="left" title="DropBox">
                                <i class="glyphicon glyphicon-th-list"></i></a>
                        </div>
                        <!-- Icon -->

                        <div class="ui-icon bg-lblue">
                            <!-- Доска -->
                            <a href="<?=Url::to(['project/view-sprint', 'id' => $project_id, 'sprint' => $item->id])?>" class="ui-tooltip" data-toggle="tooltip" data-placement="left" title="Drupal"><i class="glyphicon glyphicon-calendar"></i></a>
                        </div>
                        <!-- Icon -->

                        <!-- Button -->
                        <a href="ui-258.html#" class="ui-btn bg-lblue">+</a>
                    </div>
                
                <!-- <table class="table table-bordered">
                    <tr>
                        <th style="background-color: #007AFF !important; color: white;">Всего</th>
                        <th style="background-color: #007AFF !important; color: white;">В работе</th>
                        <th style="background-color: #007AFF !important; color: white;">Выполнено</th>
                    </tr>
                    <tr>
                        <td style="background-color: #007AFF !important; color: white;"><?php //$task_count?></td>
                        <td style="background-color: #007AFF !important; color: white;"><?php //$worked ?></td>
                        <td style="background-color: #007AFF !important; color: white;"><?php //$ready ?></td>
                    </tr>
                    <tr>
                        <td style="background-color: #007AFF !important; color: white;"><?php //$all_hour?> ч.</td>
                        <td style="background-color: #007AFF !important; color: white;"><?php //$worked_hour ?> ч.</td>
                        <td style="background-color: #007AFF !important; color: white;"><?php //$ready_hour ?> ч.</td>
                    </tr>
                </table> -->


        </div>
            </div>
            </div>
        <?php endforeach;?>
    </span>
    <?php  
        if($tip == 1 | $tip == 3 | $tip == 6) { ?>
         <div class="col-md-3">
        <li class="boards-page-board-section-list-item">
            <a href="<?=Url::to(['/ajax-board-order/createsprint', 'id' => $project_id])?>" class="board-tile mod-add" id="create-board-sprint">
            <span class="board-tile-details is-badged">
                <span title="CRM мини" dir="auto" class="board-tile-details-name">Создать новый спринт…</span>
            </span>
            </a>
        </li>
    </div>
    <?php } ?>
    </ul>
</div>
</div>
<?php
Modal::begin([
    'header' => 'Создать новую доску спринтов',
    'id' => 'board-sprint-modal'
]);
Modal::end();
?>

<script>
    <!-- Tooltip -->
    $(function () {
        $('.ui-tooltip').tooltip();
    });
    <!-- Animation -->
    $(document).ready(function(){
        $("a.ui-btn").click(function(e){
            e.preventDefault();
            if(!($(this).hasClass("active"))){
                $(this).addClass("active");
                $(this).parent(".ui-258").addClass("active");
            }
            else{
                $(this).removeClass("active");
                $(this).parent(".ui-258").removeClass("active");
            }
        });
    });
</script>