<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use kartik\sortable\Sortable;
use yii\helpers\StringHelper;
use yii\bootstrap\Modal;
use app\models\Sprints;
use app\models\Project;
use app\assets\DetailBoardSprint;
use app\models\Tasks;
use app\models\Users;
use yii\widgets\Pjax;
use yii\helpers\Html;
use app\models\StagesTasks;

$this->title = 'Этапы задач';
$tip = Yii::$app->user->identity->type;

\johnitvn\ajaxcrud\CrudAsset::register($this);
DetailBoardSprint::register($this);

$dostup = 0;
if($tip == 1 | $tip == 6 | $tip == 0) $dostup = 1;
if($tip == 3) $dostup = 1;

$sprint = Sprints::findOne($sprint_id);
$project = Project::findOne($sprint->communications_project);

?>
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">
        <a href="<?= Url::to(['/site/boards-sprint',  'project_id' => $sprint->communications_project])?>"  style="color:red;" >
            <i class="glyphicon glyphicon-backward"></i> <b>К спринтам </b>
        </a>
        <center style="margin-top: -20px; color:yellow; font-weight: bold;"> <?=$project->name ?> &nbsp (<?=$sprint->name?>) </center>
        <i style="float: right; margin-top: -20px;"> Этапы задач </i>
        </h4>
    </div>
    <div class="panel-body">

<div class="desc-page-container" id="boards-sprint-item-ajax" style="background-color: #0079bf; margin-left:-13px; margin-top: -13px; margin-bottom: -15px; margin-right: -13px;">
    <div style="margin-left: 13px;margin-top: 3px;margin-bottom: 13px;" class="panel-body-width">
        <div class="row">
            <?php if(!empty($board)):?>
            <?php foreach ($board as $key):
                $stepTitle = $key['name'];
                $stepId = $key['id'];
                $step = StagesTasks::findOne($key['id']);
                $project = Sprints::findOne($boardId)['communications_project'];
                $executor = Project::findOne($project)->executor;
                $meneger = Project::findOne($project)->responsible;
                $step_color = $step->getColorDescription();

                if($tip == 5)
                {
                    $countSteps = Tasks::find()->where(['stage' => $key['id'] , 'sprint' => $boardId, 'executor' => Yii::$app->user->identity->id ])->count();
                    $tasks = Tasks::find()->where(['stage' => $key['id'], 'sprint' => $boardId, 'executor' => Yii::$app->user->identity->id])->all();
                    
                }

                if($tip == 3)
                {
                    $sprint = Sprints::findOne($boardId);
                    $countSteps = Tasks::find()->where(['stage' => $key['id'], 'project' => $sprint['communications_project'] , 'sprint' => $boardId, 'manager_project' => Yii::$app->user->identity->id ])->count();
                    $tasks = Tasks::find()->where(['stage' => $key['id'] , 'project' => $sprint['communications_project'] , 'sprint' => $boardId, 'manager_project' => Yii::$app->user->identity->id ])->all();
                    
                } 
                if($tip == 4)
                {
                   $countSteps = Tasks::find()->where(['stage' => $key['id'] , 'sprint' => $boardId ])->count();
                   $tasks = Tasks::find()->where(['stage' => $key['id'], 'sprint' => $boardId  ])->all(); 
                }
                if($tip == 1 | $tip == 6 | $tip == 0)
                {
                   $user = Users::findOne(Yii::$app->user->identity->id);
                   if($user->creator != null)
                   {
                        $countSteps = Tasks::find()->where(['stage' => $key['id'],'sprint' => $boardId])->count();
                        $tasks = Tasks::find()->where(['stage' => $key['id'], 'sprint' => $boardId])->all();
                   }
                   else 
                   {
                        if(Yii::$app->user->identity->isSuperAdmin())
                        {
                            $countSteps = Tasks::find()->where(['stage' => $key['id'] , 'sprint' => $boardId])->count();
                            $tasks = Tasks::find()->where(['stage' => $key['id'], 'sprint' => $boardId])->all();

                        } else {
                            $countSteps = Tasks::find()->where(['stage' => $key['id'] , 'sprint' => $boardId , 'company_id' => Yii::$app->user->identity->company_id])->count();
                            $tasks = Tasks::find()->where(['stage' => $key['id'], 'sprint' => $boardId, 'company_id' => Yii::$app->user->identity->company_id])->all();
                        }
                   }

                }

            ?>
                <div class="step-column" id="<?= $key['id']?>">
                    <?php Pjax::begin(['enablePushState' => false, 'id' => 'second-pjax']) ?>
                        <div class="step-title">

                            <div class="bold-title" style="font-size: 12px;">
                                <div style="width: 100%; height:7px;" class="progress progress-striped progress-sm active m-t-5">
                                    <div class="progress-bar progress-bar-success" style="width:100%; background-color: <?=$step_color?>;">
                                    </div>
                                </div>
                                <?=StringHelper::truncate($stepTitle, 24)?> 
                                <?php if($dostup == 1){ ?>
                                    <?= Html::a('<i class="fa fa-plus"></i></a>', ['/tasks/create', 'sprint' => $sprint_id, 'project' => $project, 'executor' => $executor, 'manager' => $meneger, 'stage' =>  $key['id'], 'url' => '/site/board-detail-sprint?sprint_id='.$sprint_id ], [ 'class' => 'pull-right', 'target' => '_blank', 'title' => 'Добавить задачи', ])?> 

                                    <?= Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', ['/stages-tasks/update-step', 'id' => $key['id']], [
                                        'role'=>'modal-remote', 'title'=>'Изменить',
                                        'data-confirm'=>false, 'data-method'=>false,
                                        'data-request-method'=>'post',
                                    ]) ?>
                                    <?= Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', ['/stages-tasks/delete-step','id'=> $key['id']], [
                                        'role'=>'modal-remote', 'title'=>'Удалить',
                                        'data-confirm'=>false, 'data-method'=>false,
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Вы уверены?',
                                        'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                                    ]) ?>
                                <?php } ?>
                            </div>

                            <span class="stagevalue">
                               <span class="value">
                                   <small>Задачи <span class="count-clients-item"><?= $countSteps?></span></small>
                               </span>
                            </span>
                        </div>
                    <?php Pjax::end() ?>
                    <?php
                    $items = [];
                    if($countSteps) {
                        
                        foreach ($tasks as $value) {                            

                            $task_id = $value->id;
                            $task_name = $value->task_title;
                            $meneger = $value->managerProject->fio;
                            $proyekt = $value->project0->name;
                            if($key['color'] == 1) $color = 'red';
                            if($key['color'] == 2) $color = 'green';
                            if($key['color'] == 3) $color = 'blue';
                            if($key['color'] == 4) $color = 'yellow';
                            if($key['color'] == 5) $color = 'black';
                            if($key['color'] == 6) $color = 'white';
                            if($key['color'] == 7) $color = 'red-green';
                            $kalit = $key['id'];
                            $color = $value->getColor();
                            //echo "<br>color=".$color;//die;
                            //echo "g=".$value->priority0->getIconname();die;

                            $url = Url::to(['tasks/view', 'id' => $task_id]);
                            
                               $items[]['content'] = '
                                <div class="panel " style="margin-bottom:1px;" id="'.$task_id.'">                            
                                    <div class="panel-heading" >
                                        <div class="panel-heading-btn">
                                            <a href='.$url.' target = "_blank">
                                                <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>
                                            </a>
                                            <a href='.$url.' role="modal-remote">
                                                <i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                        <h6 class="panel-title" style="font-size:10px;"><b>Название задачи :</b><br><div class=" desc-page-container2">'.$task_name.'</div></h6>

                                        <div style="width: 90%; height:7px;" class="progress progress-striped progress-sm active m-t-5">
                                            <div class="progress-bar progress-bar-success" style="width:100%; background-color: '.$color.';">
                                            </div>
                                        </div>
                                        <div class="pull-right" style="margin-top:-7px;">
                                            <i class="'.$value->getIconname().'" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                                   ';
                        }                        
                    }

                    echo Sortable::widget([
                        'connected'=>true,
                        'options' => [
                            'data-step'=>$stepId,
                            'data-url-update' => Url::to(['ajax/update-step-client']),
                        ],
                        'items' => $items,
                        'pluginEvents' => [
                            'sortupdate' => "function(e, e2) {
                                var currentElementSelector = $(e2.item).find('div');
                                
                                var currentStep = {$stepId};
                                var currentId = currentElementSelector.attr('id');                                
                                var oldStepId =  $(currentElementSelector).data('parent-step');
                                
                                currentElementSelector.data('parent-step', currentStep + 'step');
                                                                                             
                                //Этап from
                                var oldStepObj = $('#' + oldStepId);
                                
                                var countClientsItemFrom = oldStepObj.find('.count-clients-item');
                                var countClientsItemFromValue = countClientsItemFrom.text();
                                countClientsItemFromValue--;
                                countClientsItemFrom.text(countClientsItemFromValue);
                                                              
                                //Этап to
                                var currentStepObj = $('#' + currentStep + 'step');
                                
                                var countClientsItemTo = currentStepObj.find('.count-clients-item');
                                var countClientsItemToValue = countClientsItemTo.text();
                                countClientsItemToValue++;
                                countClientsItemTo.text(countClientsItemToValue);
                                
                                $.ajax({ type: 'POST',
                                    url: '/ajax/update-step-tasks?etapId='+currentStep+'&taskId='+currentId,
                                    success: function(data){
                                        console.log(data);
                                        console.log(currentId);
                                        console.log(currentStep);
                                    }
                                });
                            }",
                        ],
                    ]);
                    ?>
                </div>
            <?php endforeach;?>
            <?php if($tip == 1 | $tip == 3 | $tip == 6){?>
                <div class="step-column add-client-button-column">
                    <a href="<?= Url::to(['ajax-step-client/create1', 'boardId' => $boardId])?>" id="create-step-sprint" class="btn">
                        Создать этапы задач
                    </a>
                </div>
            <?php } ?>
            <?php else:?>
                <?php if($tip == 1 | $tip == 3 | $tip == 6){?>
                    <div class="step-column add-client-button-column">
                        <a href="<?= Url::to(['ajax-step-client/create1', 'boardId' => $boardId])?>" id="create-step-sprint" class="btn">
                            Создать этапы задач
                        </a>
                    </div>
                <?php } ?>

            <?php endif;?>
        </div>
    </div>
</div>
 </div>
</div>

<?php
Modal::begin([
    'header' => 'Создать этапы задач',
    'id' => 'step-sprint-modal'
]);
Modal::end();
?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "modal-lg",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>