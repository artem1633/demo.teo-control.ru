<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Icons */
?>
<div class="icons-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
        ],
    ]) ?>

</div>
