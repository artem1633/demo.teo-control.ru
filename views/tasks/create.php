<?php

use yii\helpers\Html;
use app\models\Tasks;

/* @var $this yii\web\View */
/* @var $model app\models\Zalog */
$this->title = 'Новая задача';
?>
<div class="zalog-create">

    <?php if($model->scenario == Tasks::SCENARIO_BASE): ?>
        <?= $this->render('create/base', [
            'model' => $model,
        ]) ?>
    <?php endif; ?>

    <?php if($model->scenario == Tasks::SCENARIO_SECOND): ?>
        <?= $this->render('create/second', [
            'model' => $model,
            'modelUpload' => $modelUpload,
        ]) ?>
    <?php endif; ?>    
</div>