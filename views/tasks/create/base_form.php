<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Priority;
use app\models\Type;
use app\models\Project;
use app\models\Sprints;
use app\models\StagesTasks;



$tip = Yii::$app->user->identity->type;
$user = Users::findOne(Yii::$app->user->identity->id);

/*if($user->creator != null) $company = $user->creator;
else $company = Yii::$app->user->identity->id;*/
$company = $user->company_id;

$manager_project = Users::find()->where(['type' => [1,3], 'company_id' => $company ])->all();
$meneger_project=ArrayHelper::map($manager_project,'id','fio');

$executor = Users::find()->where(['type' => [1,5], 'company_id' => $company ])->all();
$executor=ArrayHelper::map($executor,'id','fio');

$author = Users::find()->all(); 
$author = ArrayHelper::map($author,'id','fio');

$priority = Priority::find()->where(['company_id' => $company ])->all();
$priority=ArrayHelper::map($priority,'id','name'); 

if($tip == 3) $projects = Project::find()->where(['responsible' => Yii::$app->user->identity->id, 'company_id' => $company ])->all();
else $projects = Project::find()->where(['is_delete' => 0, 'company_id' => $company])->all();
$project=ArrayHelper::map($projects,'id','name');

$types_directory = Type::find()->where(['company_id' => $company ])->all();
$types_directory1 = ArrayHelper::map($types_directory,'id','name');

?>

<div class="tasks-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'step')->hiddenInput()->label(false) ?>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'task_title')->textInput() ?>
            </div>
            
            <div class="col-md-4">
                <?= $form->field($model, 'types_directory')->dropDownList($types_directory1, ['prompt' => 'Выберите ']) ?>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'priority')->dropDownList($priority, ['prompt' => 'Выберите']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'project')->dropDownList(
                    $project, 
                    [
                        'prompt' => 'Выберите проект',
                        'onchange'=>'
                            $.post( "lists?id='.'"+$(this).val(), function( data ){
                                $( "select#tasks-sprint" ).html( data);
                            });
                            $.post( "lists1?id='.'"+$(this).val(), function( data ){
                                $( "select#tasks-stage" ).html( data);
                            });
                            $.post( "manager-list?id='.'"+$(this).val(), function( data ){
                                $( "select#tasks-manager_project" ).html( data);
                            });
                            $.post( "executor-list?id='.'"+$(this).val(), function( data ){
                                $( "select#tasks-executor" ).html( data);
                            });' 
                    ])
                ?>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'sprint')->dropDownList(
                    isset($model->project) ? ArrayHelper::map(Sprints::find()->where(['communications_project' => $model->project])->all(),'id','name') : ArrayHelper::map(Sprints::find()->where(['id' => 0])->all(),'id','name'),
                    ['prompt' => 'Выберите спринт',]
                    ) 
                ?>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'stage')->dropDownList(isset($model->project) ? ArrayHelper::map(StagesTasks::find()->where(['communication_id' => $model->project])->all(),'id','name') : ArrayHelper::map(StagesTasks::find()->where(['id' => 0])->all(),'id','name'), ['prompt' => 'Выберите этап']) 
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'manager_project')->dropDownList($meneger_project, ['prompt' => 'Выберите',/* 'disabled' => true,*/]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'executor')->dropDownList($executor, ['prompt' => 'Выберите исполнителя', /*'disabled' => true,*/]) ?>                
            </div>
        </div>

        <?= $form->field($model, 'author')->hiddenInput(['value'=>Yii::$app->user->identity->id])->label('') ?> 

        <div class="col-md-12" style="display: none;">
        </div> 
        
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>    

    <?php ActiveForm::end(); ?>
    
</div>
