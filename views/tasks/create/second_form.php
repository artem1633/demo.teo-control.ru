<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
//use dosamigos\datepicker\DatePicker;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Priority;
use app\models\Type;
use app\models\Project;
use mihaildev\ckeditor\CKEditor;

?>

<div class="box box-default">
    <div class="box-body" style="background-color: #ecf0f5;">
        <div class="tasks-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'author')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'task_title')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'manager_project')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'executor')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'priority')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'types_directory')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'project')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'sprint')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'stage')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'step')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'tempFiles')->hiddenInput()->label(false) ?>

            <div class="row">
                <div class="col-md-3">
                    <?php /*$form->field($model, 'start_date')->widget(
                        DatePicker::className(), [
                            'inline' => false,
                            'language' => 'ru',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])*/
                    ?>
                    <?= $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Выберите'],
                        'layout' => '{picker}{input}',
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd',
                            //'startView'=>'year',
                            'todayHighlight' => true,
                        ]
                    ]) ?>
                </div>

                <div class="col-md-3">
                    <?php /*$form->field($model, 'date_delivery')->widget(
                        DatePicker::className(), [
                            'inline' => false,
                            'language' => 'ru',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])*/
                    ?>
                    <?= $form->field($model, 'date_delivery')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Выберите'],
                        'layout' => '{picker}{input}',
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd',
                            //'startView'=>'year',
                            'todayHighlight' => true,
                        ]
                    ]) ?>
                </div>

                <div class="col-md-3">
                    <?= $form->field($model, 'time_fact')->textInput(['type' => 'number']) ?>
                </div>

                <div class="col-md-3">
                    <?= $form->field($model, 'time_scheduled')->textInput(['type' => 'number']) ?>
                </div>
            </div>

            <div class="row">
                <!-- <div class="col-md-6">
                    <?php // $form->field($model, 'commentary')->textarea(['rows' => 2]) ?>
                </div> -->

                <div class="col-md-12">
                    <?php // $form->field($model, 'description')->textarea(['rows' => 2]) ?>
                    <?php echo $form->field($model, 'description')->widget(CKEditor::className(),[
                        'editorOptions' => [
                            'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                            'inline' => false, //по умолчанию false
                            'height' => '200px',
                        ],
                    ]);
                    ?>
                </div>
            </div>
        
        	
       	  	<div class="form-group">
       	        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success' ]) ?>
       	    </div>
        	

            <?php ActiveForm::end(); ?>

                        
                            <?php $form2= ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

                            <?= $form2->field($modelUpload, 'anyFiles[]')->widget(FileInput::classname(), [
                            'options' => ['multiple' => true],
                            'pluginOptions' => [
                                'uploadUrl' => Url::to(['/ajax/upload']),
                                'uploadExtraData' => [
                                    'orderId' => $model->id,
                                ],
                                'showPreview' => true,
                                'showCaption' => true,
                                'showRemove' => false,
                                'showUpload' => true,
                                'previewFileType' => 'any',
                                'maxFileSize' => 20971520,
                            ],
                            'pluginEvents' => [
                                'fileuploaded' => "
                                    function(event, data, previewId, index) {                    
                                        var image = {
                                          name: data.files[index].name,
                                          path: data.response.path,
                                        };
                                       
                                        console.log(image);
                                        console.log(index);
                                        console.log(data.files[index].name);
                                        
                                        var resultJsonInput = $('#tasks-tempfiles').val();
                                        if(resultJsonInput.length > 0) {
                                            var resultArray = JSON.parse(resultJsonInput);
                                        } else {
                                            var resultArray = Array();
                                        }
                                        resultArray.push(index);
                                        resultArray[index] = image;
                                        var JsonResult = JSON.stringify(resultArray);
                                        $('#tasks-tempfiles').val(JsonResult);
                                    }
                                ",
                            ],
                    ]);?>
                    <?php ActiveForm::end(); ?>
                     
            
        </div>
        </div>
</div>