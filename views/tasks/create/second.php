<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\tabs\TabsX;


$items = [
    [
        'label'=>'Базовая информация',
        'headerOptions' => ['class' => 'disabled']
    ],
    [
        'label'=>'Детальная информация',
        'content'=> $this->render('second_form', ['model' => $model, 'modelUpload' => $modelUpload,]),
        'active'=> true,
    ],       
];
?>

<div class="client-form">
    <div class="box box-default">
        <div class="box-body">
    <?php 
        echo TabsX::widget([
            'items'=>$items,
            'position'=>TabsX::POS_ABOVE,
            'encodeLabels'=>false,
            'bordered'=>true,
        ]);
    ?>
</div>
</div>