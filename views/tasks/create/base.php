<?php
use kartik\tabs\TabsX;

$items = [
    [
        'label'=>'Базовая информация',
        'content'=> $this->render('base_form', ['model' => $model]),
        'active'=> true,
    ],
    [
        'label'=>'Детальная информация',
        'headerOptions' => ['class' => 'disabled']
    ],       
];
?>

<div class="client-form">
    <div class="box box-default">
        <div class="box-body">
    <?php 
        echo TabsX::widget([
            'items'=>$items,
            'position'=>TabsX::POS_ABOVE,
            'encodeLabels'=>false,
            'bordered'=>true,
        ]);
    ?>
</div>
</div>