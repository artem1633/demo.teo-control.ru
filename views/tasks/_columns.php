<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Tasksfile;

$tip = Yii::$app->user->identity->type; $create = 0;
if($tip == 1 | $tip == 3 | $tip == 6 | $tip == 0) $create = 1;

$data = \yii\helpers\ArrayHelper::map(\app\models\Companies::find()->with('admin')->all(), 'id', 'admin.fio');

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'task_title',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'author',
        'value' => 'author0.fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'manager_project',
        'value' => 'managerProject.fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'executor',
        'value' => 'executor0.fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'priority',
        'value' => 'priority0.name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'project0.name',
    ],
    [
        'attribute' => 'files',
        //'role' => 'modal-remote',
        'format' => 'html',
        'value' => function($data){
            return Tasksfile::getFilesContent($data->id);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Компания',
        'attribute' => 'company_id',
        'value' => 'company.admin.fio',
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'filterWidgetOptions' => [
            'data' => $data,
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ],
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'stage',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'sprint',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'types_directory',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'commentary',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'file',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'description',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'date_delivery',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'time_fact',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'time_scheduled',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'company',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => $create == 1 ? '{view}{delete}' : '{view}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'view' => function ($url, $model) {
                return Html::a('<i class="fa fa-eye text-primary" style="font-size: 16px;"></i>', $url, [
                    /*'role'=>'modal-remote',*/ 'data-pjax' => 0, 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   