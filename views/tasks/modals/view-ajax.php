<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\Users;
use teo_crm\yii\module\Comments;
use yii\helpers\HtmlPurifier;

\johnitvn\ajaxcrud\CrudAsset::register($this);
$this->title = 'Просмотр/Изменение';

$tip = Yii::$app->user->identity->type;
?>
    
        <div class="row">
            <div class="col-md-12">
                <h3> Общие элементы  </h3>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td><b><?=$model->getAttributeLabel('task_title')?></b></td>
                                <td colspan="3"><?=Html::encode($model->task_title)?></td>
                            </tr>
                            <tr>
                                <td><b><?=$model->getAttributeLabel('manager_project')?></b></td>
                                <td><?=Html::encode($model->managerProject->fio)?></td>
                                <td><b><?=$model->getAttributeLabel('executor')?></b></td>
                                <td><?= $tip == 4 ? '' : Html::encode($model->executor0->fio)?></td>
                            </tr>
                            <tr>
                                <td><b><?=$model->getAttributeLabel('priority')?></b></td>
                                <td><?=Html::encode($model->priority0->name)?></td>
                                <td><b><?=$model->getAttributeLabel('types_directory')?></b></td>
                                <td><?= $model->typesDirectory->name?></td>
                            </tr>
                            <tr>
                                <td><b><?=$model->getAttributeLabel('project')?></b></td>
                                <td><?=Html::encode($model->project0->name)?></td>
                                <td><b><?=$model->getAttributeLabel('sprint')?></b></td>
                                <td><?= $model->sprint0->name?></td>
                            </tr>
                            <tr>
                                <td><b><?=$model->getAttributeLabel('author')?></b></td>
                                <td><?= $model->author0->fio?></td>
                                <td><b><?=$model->getAttributeLabel('stage')?></b></td>
                                <td><?=Html::encode($model->stage0->name)?></td>
                            </tr>                                    
                        </tbody>
                    </table>
                </div>
            </div>
                   
            <div class="col-md-12">
                <h3> Дополнительние параметры</h3>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td><b><?=$model->getAttributeLabel('start_date')?></b></td>
                            <td><?= $model->start_date?></td>
                            <td><b><?=$model->getAttributeLabel('date_delivery')?></b></td>
                            <td><?= $model->date_delivery?></td>
                            <td><b><?=$model->getAttributeLabel('time_fact')?></b></td>
                            <td><?=Html::encode($model->time_fact)?></td>
                            <td><b><?=$model->getAttributeLabel('time_scheduled')?></b></td>
                            <td><?=Html::encode($model->time_scheduled)?></td>
                        </tr>
                        <tr>
                            <td><b><?=$model->getAttributeLabel('description')?></b></td>
                            <td colspan="7"><?php echo HtmlPurifier::process($model->description, ['Attr.EnableID' => true,]);?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>