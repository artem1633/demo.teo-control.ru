<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
//use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Priority;
use app\models\Type;
use app\models\Project;
use mihaildev\ckeditor\CKEditor;
use kartik\date\DatePicker;

?>

<div class="box box-default">
    <div class="box-body" style="background-color: #ecf0f5;">
        <div class="tasks-form">

            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Выберите'],
                        'layout' => '{picker}{input}',
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd',
                            //'startView'=>'year',
                            'todayHighlight' => true,
                        ]
                    ]) ?>
                    <?php /*$form->field($model, 'start_date')->widget(
                        DatePicker::className(), [
                            'inline' => false,
                            'language' => 'ru',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])*/
                    ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'date_delivery')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Выберите'],
                        'layout' => '{picker}{input}',
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd',
                            //'startView'=>'year',
                            'todayHighlight' => true,
                        ]
                    ]) ?>
                    <?php /*$form->field($model, 'date_delivery')->widget(
                        DatePicker::className(), [
                            'inline' => false,
                            'language' => 'ru',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])*/
                    ?>
                </div>

                <div class="col-md-3">
                    <?= $form->field($model, 'time_fact')->textInput(['type' => 'number']) ?>
                </div>

                <div class="col-md-3">
                    <?= $form->field($model, 'time_scheduled')->textInput(['type' => 'number']) ?>
                </div>
            </div>

            <div class="row">
               <!--  <div class="col-md-6">
                   <?php // $form->field($model, 'commentary')->textarea(['rows' => 2]) ?>
               </div> -->

                <div class="col-md-12">
                    <?php // $form->field($model, 'description')->textarea(['rows' => 2]) ?>
                    <?php echo $form->field($model, 'description')->widget(CKEditor::className(),[
                        'editorOptions' => [
                            'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                            'inline' => false, //по умолчанию false
                            'height' => '200px',
                        ],
                    ]);
                    ?>
                </div>
            </div>
    
            <?php ActiveForm::end(); ?>
            
        </div>
        </div>
</div>