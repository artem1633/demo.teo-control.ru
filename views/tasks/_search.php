<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TasksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tasks-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'author') ?>

    <?= $form->field($model, 'manager_project') ?>

    <?= $form->field($model, 'executor') ?>

    <?= $form->field($model, 'priority') ?>

    <?php // echo $form->field($model, 'project') ?>

    <?php // echo $form->field($model, 'stage') ?>

    <?php // echo $form->field($model, 'sprint') ?>

    <?php // echo $form->field($model, 'types_directory') ?>

    <?php // echo $form->field($model, 'commentary') ?>

    <?php // echo $form->field($model, 'file') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'date_delivery') ?>

    <?php // echo $form->field($model, 'time_fact') ?>

    <?php // echo $form->field($model, 'time_scheduled') ?>

    <?php // echo $form->field($model, 'task_title') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
