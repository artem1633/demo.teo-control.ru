<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;
use app\assets\AjaxClientsAsset;
use yii\helpers\ArrayHelper;


AjaxClientsAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
/* @var $form yii\widgets\ActiveForm */

 $author = \app\models\User::find()->all(); 
 $author=ArrayHelper::map($author,'id','username');
 $manager_project = \app\models\User::find()->all(); 
 $meneger_project=ArrayHelper::map($manager_project,'id','username'); 
 $executor = \app\models\User::find()->all(); 
 $executor=ArrayHelper::map($executor,'id','username'); 
 $priority = \app\models\Priority::find()->all(); 
 $priority=ArrayHelper::map($priority,'id','name'); 
 $project = \app\models\Project::find()->all(); 
 $project=ArrayHelper::map($project,'id','name');
 $stages_tasks = \app\models\StagesTasks::find()->all(); 
 $stagestasks=ArrayHelper::map($stages_tasks,'id','name');
 $sprints = \app\models\Sprints::find()->all(); 
 $sprints=ArrayHelper::map($sprints,'id','name');
 $types_directory = \app\models\Type::find()->all(); 
 $types_directory1=ArrayHelper::map($types_directory,'id','name');
 
?>


<div class="tasks-form">

    <?php $form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
            ]
    ]) ?>

    <?= $form->field($model, 'tempFiles')->hiddenInput()->label(false) ?>
<div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                     <?= $form->field($model, 'task_title')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'project')
                        ->dropDownList($project, ['prompt' => 'Выберите проект', 'disabled' => true,]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'manager_project')
                        ->dropDownList($meneger_project, ['prompt' => 'Выберите менеджер проекта']) ?>
                </div>
                <div class="col-md-6">
                        <?= $form->field($model, 'priority')
                        ->dropDownList($priority, ['prompt' => 'Выберите приоритет']) ?>
                </div>
                <div class="col-md-6">
                        <?= $form->field($model, 'author')
                        ->dropDownList($author, ['prompt' => 'Выберите автора']) ?>
                </div>
                <div class="col-md-6">
                     <?= $form->field($model, 'executor')
                        ->dropDownList($executor, ['prompt' => 'Выберите исполнителя']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                        <?= $form->field($model, 'stage')
                        ->dropDownList($stagestasks, ['prompt' => 'Выберите этап']) ?>
                </div>
            
                <div class="col-md-4">
                        <?= $form->field($model, 'sprint')
                        ->dropDownList($sprints, ['prompt' => 'Выберите спринт', 'disabled' => true,]) ?>
                </div>
            
                <div class="col-md-4">
                        <?= $form->field($model, 'types_directory')
                        ->dropDownList($types_directory1, ['prompt' => 'Выберите справочник ']) ?>
                </div>
            </div> 
            <div class="row">
                <div class="col-md-6">
                        <?= $form->field($model, 'commentary')->textarea(['rows' => 4]) ?>
                </div>
            
                <div class="col-md-6">
                        <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>
                </div>
            <div class="col-md-4">   
                <?= $form->field($model, 'date_delivery')->widget(
                DatePicker::className(), [
                'inline' => false,
                'language' => 'ru',
                //'template' => '<div class="well well-sm" style="background-color: #ifff; width:250px">(дата)</div>',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);?>
            </div>
            <div class="col-md-4">   
                <?= $form->field($model, 'time_fact')->widget(
                DatePicker::className(), [
                'inline' => false,
                'language' => 'ru',
                //'template' => '<div class="well well-sm" style="background-color: #ifff; width:250px">(дата)</div>',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);?>
            </div>
            <div class="col-md-4">   
                <?= $form->field($model, 'time_scheduled')->widget(
                DatePicker::className(), [
                'inline' => false,
                'language' => 'ru',
               //'template' => '<div class="well well-sm" style="background-color: #ifff; width:250px">(дата)</div>',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);?>
            </div>
            </div>

            <div class="form-group">
               <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
     <?php ActiveForm::end(); ?>
            <div class="col-md-12">
                    <?php $form2= ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

                    <?= $form2->field($modelUpload, 'anyFiles[]')->widget(FileInput::classname(), [
                    'options' => ['multiple' => true],
                    'pluginOptions' => [
                        'uploadUrl' => Url::to(['/ajax/upload']),
                        'uploadExtraData' => [
                            'orderId' => $model->id,
                        ],
                        'showPreview' => true,
                        'showCaption' => true,
                        'showRemove' => false,
                        'showUpload' => true,
                        'previewFileType' => 'any',
                        'maxFileSize' => 20971520,
                    ],
                    'pluginEvents' => [
                        'fileuploaded' => "
                            function(event, data, previewId, index) {                    
                                var image = {
                                  name: data.files[index].name,
                                  path: data.response.path,
                                };
                               
                                console.log(image);
                                console.log(index);
                                console.log(data.files[index].name);
                                
                                var resultJsonInput = $('#tasks-tempfiles').val();
                                if(resultJsonInput.length > 0) {
                                    var resultArray = JSON.parse(resultJsonInput);
                                } else {
                                    var resultArray = Array();
                                }
                                resultArray.push(index);
                                resultArray[index] = image;
                                var JsonResult = JSON.stringify(resultArray);
                                $('#tasks-tempfiles').val(JsonResult);
                            }
                        ",
                    ],
            ]);?>
            <?php ActiveForm::end(); ?>
            </div>     
     </div>
   </div>

</div>

