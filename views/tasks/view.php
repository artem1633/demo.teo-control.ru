<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\Users;
use teo_crm\yii\module\Comments;
use yii\helpers\HtmlPurifier;



\johnitvn\ajaxcrud\CrudAsset::register($this);

$this->title = 'Просмотр/Изменение';

$dostup = 0;
$tip = Yii::$app->user->identity->type;
$user_id = Yii::$app->user->identity->id;

if($tip == 6 | $tip == 1 | $tip == 3 | $tip == 0) $dostup = 1;
//$dostup = 1;
?>
    
    <div class="box box-default">
        <div class="box-body">
<div class="clients-view">

    <div class="row">

        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#default-tab-1" data-toggle="tab" aria-expanded="true">
                        <span class="visible-xs">Общая информация</span>
                        <span class="hidden-xs">Общая информация</span>
                    </a>
                </li>
                <li class="">
                    <a href="#default-tab-2" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs">Дополнительние параметры</span>
                        <span class="hidden-xs">Дополнительние параметры</span>
                    </a>
                </li>
                <?php if($dostup == 1) { ?>
                <li class="">
                    <a href="#default-tab-3" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs">История изменений</span>
                        <span class="hidden-xs">История изменений</span>
                    </a>
                </li>
                <?php } ?>
                <li class="">
                    <a href="#default-tab-4" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs">Комменты </span>
                        <span class="hidden-xs">Комменты </span>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="default-tab-1">
                    <div class="row">
                        <?php Pjax::begin(['enablePushState' => false, 'id' => 'base-pjax']) ?>
                            <div class="col-md-11" style="margin-left: 40px;"><br>
                            <h3>
                                Общие элементы 
                                <?php if($dostup == 1){ ?>
                                <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['edit-base', 'id' => $model->id])?>"><i class="fa fa-pencil"></i></a>
                                <?php } ?>
                                
                            </h3>
                            <div style="margin-top: 10px" class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('task_title')?></b></td>
                                        <td colspan="3"><?=Html::encode($model->task_title)?></td>
                                        
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('manager_project')?></b></td>
                                        <td><?=Html::encode($model->managerProject->fio)?></td>
                                        <td><b><?=$model->getAttributeLabel('executor')?></b></td>
                                        <td><?= $tip == 4 ? '' : Html::encode($model->executor0->fio)?></td>
                                    </tr>
                                    <tr>

                                    <td><b><?=$model->getAttributeLabel('priority')?></b></td>
                                        <td><?=Html::encode($model->priority0->name)?></td>
                                        <td><b><?=$model->getAttributeLabel('types_directory')?></b></td>
                                        <td><?= $model->typesDirectory->name?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('project')?></b></td>
                                        <td><?=Html::encode($model->project0->name)?></td>
                                        <td><b><?=$model->getAttributeLabel('sprint')?></b></td>
                                        <td><?= $model->sprint0->name?></td>
                                    </tr>
                                    <tr>
                                        <td><b><?=$model->getAttributeLabel('author')?></b></td>
                                        <td><?= $model->author0->fio?></td>
                                        <td><b><?=$model->getAttributeLabel('stage')?></b></td>
                                        <td><?=Html::encode($model->stage0->name)?></td>
                                    </tr>                                                            
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php Pjax::end() ?>                        
                    </div>
                </div>
                <div class="tab-pane fade" id="default-tab-2">
                    <div class="row">
                   
                        <?php Pjax::begin(['enablePushState' => false, 'id' => 'second-pjax']) ?>
                            <div class="col-md-11" style="margin-left: 40px;"><br>
                                <h3>
                                    Дополнительние параметры
                                    <?php if($dostup == 1 | $tip == 5){ ?>
                                     <a class="btn btn-primary" role="modal-remote" href="<?=Url::toRoute(['edit-second', 'id' => $model->id])?>"><i class="fa fa-pencil"></i></a>
                                     <?php } ?>
                                    
                                </h3>
                                <div style="margin-top: 10px" class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td><b><?=$model->getAttributeLabel('start_date')?></b></td>
                                            <td><?= $model->start_date?></td>
                                            <td><b><?=$model->getAttributeLabel('date_delivery')?></b></td>
                                            <td><?= $model->date_delivery?></td>
                                            <td><b><?=$model->getAttributeLabel('time_fact')?></b></td>
                                            <td><?=Html::encode($model->time_fact)?></td>
                                            <td><b><?=$model->getAttributeLabel('time_scheduled')?></b></td>
                                            <td><?=Html::encode($model->time_scheduled)?></td>
                                            
                                        </tr>
                                        
                                        <tr>
                                            <!-- <td><b><?php // $model->getAttributeLabel('commentary')?></b></td>
                                            <td colspan="2"><?php // Html::encode($model->commentary)?></td> -->
                                            <td><b><?=$model->getAttributeLabel('description')?></b></td>
                                            <td colspan="7"><?php echo HtmlPurifier::process($model->description, ['Attr.EnableID' => true,]);?></td>
                                            
                                        </tr>                                                        
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php Pjax::end() ?>                 
                    </div>
                </div>
                <?php if($dostup == 1) { ?>
                <div class="tab-pane fade" id="default-tab-3">
                    <div class="row">
                        <div class="col-md-12" style="margin-left: 5px;">
                            <br><br>
                            <?php $changes =\app\models\Changing::find()->where(['table_name' => 'task', 'line_id' => $model->id])->all();
                                if($changes == null){ echo '<span style = "color:red; font-size:22px;"><center> <b>Нет изменений </b></center></span>';}
                                else {
                            ?> 
                            <table class="table table-bordered table-condensed">
                                <tr>
                                    <th>Статус</th>
                                    <th>Дата/Время</th>
                                    <th>Пользователь (Логин)</th>
                                    <th>Поле</th>
                                    <th>Значение</th>
                                    <th>Изменение</th>
                                </tr>
                                <?php 
                                foreach ($changes as $change) {
                                    $username = Users::findOne($change->user_id);
                                ?>
                                <tr>
                                    <td><?=$change->status?></td>
                                    <td><?=\Yii::$app->formatter->asDate($change->date_time, 'php:H:i, d.m.Y')?></td>
                                    <td><?=$username->login?></td>
                                    <td><?=$change->fields?></td>
                                    <td><?=$change->old_value?></td>
                                    <td><?=$change->new_value?></td>
                                </tr>
                                <?php } ?>
                            </table>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="tab-pane fade" id="default-tab-4">
                    <?php echo Comments\widgets\CommentListWidget::widget(['entity' => (string) $model->id,]);?>
                </div>
                
            </div>
        </div>
    </div>
</div>
</div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    "size" => "modal-lg",
    "options" => [
        "open.bs.modal" => "function(){ console.log('123'); }",
        "tabindex" => false,
    ],
])?>
<?php Modal::end(); ?>