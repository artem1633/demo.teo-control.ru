<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Technologies */

?>
<div class="technologies-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
