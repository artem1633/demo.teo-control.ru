<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Technologies */
?>
<div class="technologies-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'title',
            //'updated_at',
            'created_at',
            //'company',
        ],
    ]) ?>

</div>
