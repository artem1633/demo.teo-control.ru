<?php

use yii\widgets\DetailView;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fio',
            'login',
            'telephone',
            'city',
            [
                'attribute' => 'type',
                'value' => function ($data) {
                   if($data->type == 1) return 'Администратор';
                   if($data->type == 2) return 'Аналитик';
                   if($data->type == 3) return 'Менеджер проекта';
                   if($data->type == 4) return 'Клиент';
                   if($data->type == 5) return 'Програмист';
                   if($data->type == 6) return 'Компания';
                },
            ],
            [
                'attribute' => 'dostup',
                'value' => function ($data) {
                   if($data->dostup == 1) return 'ВКЛ';
                   else return 'ВЫКЛ';
                },
            ],
            //'technology',
            'cost',
            'otsenka_opit',
            'opit_god',
            [
                'attribute' => 'creator',
                'value' => function ($data) {
                    return Users::findOne($data->creator)->fio;
                },
            ],
            //'company',
        ],
    ]) ?>

</div>
