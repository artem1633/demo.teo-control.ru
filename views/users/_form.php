<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Technologies;
use yii\helpers\ArrayHelper;

if($model->isNewRecord) $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/nouser.jpg';
else{
    if($model->avatar == '' || $model->avatar == null) $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/nouser.jpg';
    else $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.$model->id.'.jpg';
}

?>

<div class="users-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-3 vcenter">
            <?=Html::img($path, [
                'style' => 'width:180px; height:180px;',
                'class' => 'img-circle',
            ])?>
        </div>
        <div class="col-md-6 vcenter">
             <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3 vcenter">
             <?= $form->field($model, 'type')->dropDownList($model->getType(), ['prompt' => 'Выберите тип']) ?>
        </div>
        <div class="col-md-3 vcenter">
             <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
        </div>
        <?php if($model->isNewRecord) { ?>    
            <div class="col-md-3 vcenter">
                <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>
            </div>  
        <?php }
        else { ?>
            <div class="col-md-3 vcenter">
                 <?= $form->field($model, 'new_parol')->textInput(['maxlength' => true]) ?>
            </div>
        <?php } ?>
        <div class="col-md-3 vcenter">
             <?= $form->field($model, 'vk_href')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3 vcenter">
             <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3 vcenter">
             <?= $form->field($model, 'dostup')->dropDownList($model->getDostup(), []) ?>
        </div>
        <div class="col-md-3 vcenter">
             <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
        </div>
        <?= $form->field($model, 'creator')->hiddenInput()->label(false) ?>
    
        <div class="col-md-3 vcenter">
            <?= $form->field($model, 'file')->fileInput(); ?>
        </div>
        <div class="col-md-3 vcenter">
            <?= $form->field($model, 'otsenka_opit')->textInput(['type' => 'number', 'disabled' => $model->type==4 | $model->type==null ? true :false] ) ?>
        </div>
    
        <div class="col-md-3 vcenter">
             <?= $form->field($model, 'opit_god')->textInput(['type' => 'number', 'disabled' => $model->type==4 | $model->type==null ? true :false]) ?>
        </div>
        <div class="col-md-3 vcenter">
             <?= $form->field($model, 'cost')->textInput(['type' => 'number', 'disabled' => $model->type==4 | $model->type==null ? true :false]) ?>
        </div>
        <div class="col-md-12 vcenter">            
            <?= $form->field($model, 'selectTechnologies')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->allTechnologies, 
                'value' => $model->selectTechnologies,
                'options' => [
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div> 
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel" style="background-color:#d9e0e7">
                <div class="panel-heading" >
                    <div class="panel-heading-btn">                    
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-plus"></i>
                        </a>
                    </div>
                    <h4 class="panel-title"><b>Инструкция по получению Телеграм чат ИД</b><br></h4>
                </div>
                <div class="panel-body p-0 page-container-width2 desc-page-container" style="display: none;">
                        1 - Нужно зайти в бот <a style="width: 100px;" target="_blank" href="https://t.me/teo_control_bot">https://t.me/teo_control_bot</a> и нажать кнопку старт<br>

                        2 - Нужно зайти в бот <a style="width: 100px;" target="_blank" href="https://t.me/id_chatbot">https://t.me/id_chatbot</a> и нажать кнопку старт и полученный код<br>

                        3 - Вставить в своем профиле полученный id в поле Телеграм чат ИД<br>                           
                </div>
            </div>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>

</div>


<?php 
$this->registerJs(<<<JS
    $('select#users-type').on('change', function() 
    { var a = $('select#users-type').val();
        if(a == 4)
        {
         $('#input_technologies').prop("disabled", true).focus();
         $('#users-cost').prop("disabled", true).focus();
         $('#users-otsenka_opit').prop("disabled", true).focus();
         $('#users-opit_god').prop("disabled", true).focus();
        }
        else
        { 
            $('#input_technologies').prop("disabled", false).focus();
            $('#users-cost').prop("disabled", false).focus();
            $('#users-otsenka_opit').prop("disabled", false).focus();
            $('#users-opit_god').prop("disabled", false).focus();
         }
     }
);
JS
);
?>