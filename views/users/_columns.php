<?php
use yii\helpers\Url;
use yii\helpers\Html;

$data = \yii\helpers\ArrayHelper::map(\app\models\Companies::find()->with('admin')->all(), 'id', 'admin.fio');

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'login',
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'password',
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telephone',
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'city',
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'content' => function ($data) {
           if($data->type == 1) return 'Администратор';
           if($data->type == 2) return 'Аналитик';
           if($data->type == 3) return 'Менеджер проекта';
           if($data->type == 4) return 'Клиент';
           if($data->type == 5) return 'Програмист';
           if($data->type == 6) return 'Компания';
       },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dostup',
        'content' => function ($data) {
           if($data->dostup == 1) return 'ВКЛ';
           if($data->dostup == 2) return 'ВЫКЛ';                   
        },
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'comment',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'technology',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'cost',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'otsenka_opit',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'opit_god',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'creator',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'company',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Компания',
        'attribute' => 'company_id',
        'value' => 'company.admin.fio',
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'filterWidgetOptions' => [
            'data' => $data,
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                if ($model->type != 6){
                    return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Удалить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-confirm-title'=>'Вы уверены?',
                        'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                    ]);
                }
                
            },
            'update' => function ($url, $model) {
                if ($model->type != 6){
                        return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role'=>'modal-remote', 'title'=>'Изменить',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                    ])."&nbsp;";
                }               
            }
        ],
    ],

];   