<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rates */
?>
<div class="rates-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
