<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;

?>
<div class="site-error">

    <!-- begin error -->
    <div class="error">
        <div class="error-code m-b-10"><?=$exception->name?> <i class="fa fa-warning"></i></div>
        <div class="error-content">
            <div class="error-message"><?=$exception->getMessage()?></div>
            <div class="error-desc m-b-20">
                Если это ошибка сервера. <br>
                Свяжитесь с тех поддержкой.
            </div>
        </div>
    </div>
    <!-- end error -->

</div>
