<?php

use yii\helpers\Url;

$tip = Yii::$app->user->identity->type;

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Рабочий стол', 'icon' => 'fa fa-desktop', 'url' => ['site/dashboard'], 'visible' => $tip != 2 ],
                    ['label' => 'Доска проекта', 'icon' => 'fa fa-table', 'url' => ['site/boards-project'], 'visible' => $tip != 2 ],
                    ['label' => 'Задачи', 'icon' => 'fa fa-building', 'url' => ['/tasks/index'], 'visible' => ($tip == 0 || $tip == 1) ],
                    ['label' => 'Пользователи', 'icon' => 'fa fa-users', 'url' => ['users/index'], 'visible' => ($tip == 0 | $tip == 1 | $tip ==2 | $tip == 6) ],
                    //['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    //['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Настройки',
                        'icon' => 'fa fa-book',
                        'url' => '#',
                        'options' => ['class' => 'has-sub'],
                        'visible' => $tip != 5,
                        'items' => [

                            /*['label' => 'Проект', 'icon' => 'fa fa-tags', 'url' => ['/project/index'],
                                'visible' => ($tip == 0 | $tip == 1 | $tip ==4 | $tip == 3 | $tip == 6) ],
                            ['label' => 'Спринт', 'icon' => 'fa fa-book', 'url' => ['/sprints/index'], 'visible' => ($tip == 0 | $tip == 1 | $tip == 3 | $tip == 6) ],*/
                            ['label' => 'Приоритет', 'icon' => 'fa fa-book', 'url' => ['/type/index'], 'visible' => ($tip == 0 | $tip == 1 | $tip == 6) ],
                            ['label' => 'Навыки', 'icon' => 'fa fa-book', 'url' => ['technologies/index'], 'visible' => ($tip == 0 | $tip == 1 | $tip == 6) ],
                            ['label' => 'Справочник типов', 'icon' => 'fa fa-book', 'url' => ['/priority/index'], 'visible' => ($tip == 0 | $tip == 1 | $tip == 6) ],
                            //['label' => 'Иконки', 'icon' => 'fa fa-book', 'url' => ['icons/index'], 'visible' => ($tip == 0 | $tip == 1 | $tip == 6) ],
                        ],
                    ],
                    [
                        'label' => 'Администратор',
                        'icon' => 'fa fa-book',
                        'url' => '#',
                        'options' => ['class' => 'has-sub'],
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                        'items' => [
                            ['label' => 'Компании', 'icon' => 'fa fa-book', 'url' => ['companies/index']],
                            ['label' => 'Статистика', 'icon' => 'fa fa-book', 'url' => ['statistic/index']],
                            ['label' => 'Логи', 'icon' => 'fa fa-book', 'url' => ['logs/index']],
                            ['label' => 'Тарифы', 'icon' => 'fa fa-book', 'url' => ['rates/index']],
                            ['label' => 'Шаблоны сообщений', 'icon' => 'fa fa-book', 'url' => ['email-templates/index']],
                        ],
                    ],
                    ['label' => 'Инструкция', 'url' => ['/site/instruksion'], ],
                    ['label' => 'Выйти', 'url' => ['/site/logout'], ],

                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
