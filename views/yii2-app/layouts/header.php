<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 

CrudAsset::register($this);
?>
<div id="ajaxCrudDatatable">
<div id="header" class="header navbar navbar-default navbar-fixed-top">
    <!-- begin container-fluid -->
    <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand"><span class="navbar-logo"></span> Control Project</a>
            <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- end mobile sidebar expand / collapse button -->

        <!-- begin header navigation right -->
        <ul class="nav navbar-nav navbar-right">
            <li>
                <?php if(Yii::$app->user->isGuest === false): ?>
                    <?=Html::a('Лицензия до '.Yii::$app->formatter->asDate(Yii::$app->user->identity->getCompanyInstance()->access_end_datetime, 'php:d.m.Y'), ['#'])?>
                <?php endif; ?>
            </li>
            <!-- <li><?php /*Html::a('Инструкция', 
                    ['/site/instruksion'], 
                    [
                    ]
                )*/ ?>
            </li> -->
            <li><?= Html::a('Профиль', 
                    ['/users/change', 'id' => Yii::$app->user->identity->id], 
                    [
                        'role'=>'modal-remote', 'title'=>'Изменить профиля',
                        'data-confirm'=>false, 'data-method'=>false,
                        'data-request-method'=>'post',
                    ]
                )?>
            </li>
            <?php if(Yii::$app->user->isGuest === false && Yii::$app->user->identity->isSuperAdmin()): ?>
                <li>
                    <?= Html::a(
                        '<i class="fa fa-cog fa-lg"></i>',
                        ['/settings'],
                        ['title' => 'Настройки']
                    ) ?>
                </li>
            <?php endif; ?>
            <!-- <li class="dropdown navbar-user">
                <?php /*Html::a(
                    'Выйти',
                    ['/site/logout'],
                    ['data-method' => 'post']
                )*/ ?>
            </li> -->
        </ul>
        <!-- end header navigation right -->
    </div>
    <!-- end container-fluid -->
</div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "modal-lg",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
