<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
?>
<div class="project-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'creator_composer',
            'responsible',
            'status',
            'client',
            //'company',
        ],
    ]) ?>

</div>
