<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Users;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */

$user = Users::findOne(Yii::$app->user->identity->id);

$company_id = $user->company_id;

$responsible = Users::find()->where(['type' => [1,3], 'company_id' => $company_id])->all();
$responsible = ArrayHelper::map($responsible,'id','fio');

$executors = Users::find()->where(['type' => [1,5], 'company_id' => $company_id])->all();
$executors = ArrayHelper::map($executors,'id','fio');

$client = Users::find()->where(['type' => 4, 'company_id' => $company_id])->all();
$clients = ArrayHelper::map($client,'id','fio');


?>

<div class="project-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'responsible')->dropDownList($responsible, ['prompt'=>'Выберите ответственного']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'client')->dropDownList($clients, ['prompt'=>'Выберите клиента']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'sorting')->textInput(['type' => 'number']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'executor')->dropDownList($executors, ['prompt'=>'Выберите исполнителя']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model->getStatus(), ['prompt'=>'Выберите статуса']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'is_delete')->dropDownList($model->getDeleted(), ['prompt'=>'Выберите']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'description')->widget(CKEditor::className(),[
                'editorOptions' => [
                'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false, //по умолчанию false
                    'height' => '120px',
                ],
            ]);
            ?>
        </div>
    </div>


    <div class="col-md-6" style="display: none;">
        <?= $form->field($model, 'creator_composer')->textInput(['value'=>Yii::$app->user->identity->id]) ?>
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
