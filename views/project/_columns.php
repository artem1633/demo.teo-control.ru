<?php
use yii\helpers\Url;
use yii\helpers\Html;

$tip = Yii::$app->user->identity->type; $create = 0;
if($tip == 1 | $tip == 3 | $tip == 6 | $tip == 0) $create = 1;

$data = \yii\helpers\ArrayHelper::map(\app\models\Companies::find()->with('admin')->all(), 'id', 'admin.fio');

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'creator_composer',
        'value' => 'creatorComposer.fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'executor',
        'value' => 'executor0.fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'responsible',
        'value' => 'responsible0.fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'content'=>function($data){
            if ($data->status == 1){ return 'Выполнено'; }
            if ($data->status == 2){ return 'Не выполнено'; }
            if ($data->status == 3){ return 'Работает'; }
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client',
        'value' => 'client0.fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Компания',
        'attribute' => 'company_id',
        'value' => 'company.admin.fio',
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'filterWidgetOptions' => [
            'data' => $data,
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'is_delete',
        'content'=>function($data){
            if ($data->is_delete == 1){ return 'Удалено'; }
            if ($data->is_delete == 0){ return 'Не удалено'; }            
        },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => $create == 1 ? '{update}{delete}' : '',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   