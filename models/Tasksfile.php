<?php

namespace app\models;

use app\base\AppActiveQuery;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;
use yii\helpers\Url;


/**
 * This is the model class for table "tasksfile".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $title
 * @property string $path
 *
 * @property Tasks $order
 */
class Tasksfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tasksfile';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'integer'],
            [['path'], 'string'],
            [['title'], 'string', 'max' => 25],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tasks::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'title' => 'Title',
            'path' => 'Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Tasks::className(), ['id' => 'order_id']);
    }

    public static function getPathes($orderId) {
        $pathes = self::find()->select('path')->asArray()->all();
        $res = [];
        foreach ($pathes as $path) {
            $res[] = DIRECTORY_SEPARATOR . $path['path'];
        }

        return $res;
    }
// через эту функцию можно скачать файлов задач 
    public static function getFilesContent($orderId) {
        $resArr = self::find()->where(['order_id' => $orderId])->asArray()->all();

        $str = "<div class='files-content'>";
        foreach ($resArr as $item) {
            $id = $item['id'];
            $title = $item['title'];
            $path = Url::to(['tasks/download', 'path' => $item['path']]);
            $removeUrl = Url::to(['/ajax/remove-file', 'id' => $id]);
            $str .= "<div class='file-item'>
                        <a href='{$path}' id='{$id}' class='filename'>{$title}</a>
                        <a href='{$removeUrl}' class='remove-file'><i class='fa fa-times' aria-hidden='true'></i></a>
                     </div>";
        }

        $str .= "</div>";

        return $str;
    }
}
