<?php

namespace app\models;

use app\base\AppActiveQuery;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\HtmlPurifier;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "tasks".
 *
 * @property integer $id
 * @property integer $author
 * @property integer $manager_project
 * @property integer $executor
 * @property integer $priority
 * @property integer $project
 * @property integer $stage
 * @property integer $sprint
 * @property integer $types_directory
 * @property string $commentary
 * @property string $file
 * @property string $description
 * @property string $date_delivery
 * @property string $time_fact
 * @property string $time_scheduled
 * @property string $task_title
 *
 * @property Project $project0
 * @property Author $author0
 * @property Executor $executor0
 * @property MenegerProject $managerProject
 * @property Priority $priority0
 * @property Sprints $sprint0
 * @property StagesTasks $stage0
 * @property Type $typesDirectory
 */
class Tasks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const SCENARIO_BASE = 'BASE';
    const SCENARIO_SECOND = 'SECOND';

    public $tempFiles;
    public $step;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tasks';
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_BASE] = ['author','task_title', 'manager_project', 'executor', 'types_directory', 'project', 'sprint', 'priority', 'stage'];

        $scenarios[self::SCENARIO_SECOND] = ['commentary','description', 'date_delivery','time_fact', 'time_scheduled', 'tempFiles', 'start_date'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author', 'manager_project', 'executor', 'priority', 'project', 'stage', 'sprint', 'types_directory'], 'integer'],
            [['commentary', 'description'], 'string'],
            [['date_delivery', 'time_fact', 'time_scheduled', 'start_date'], 'safe'],
            [['file', 'task_title'], 'string', 'max' => 255],
            [['project'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project' => 'id']],
            [['author'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author' => 'id']],
            [['executor'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['executor' => 'id']],
            [['manager_project'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['manager_project' => 'id']],
            [['priority'], 'exist', 'skipOnError' => true, 'targetClass' => Priority::className(), 'targetAttribute' => ['priority' => 'id']],
            [['sprint'], 'exist', 'skipOnError' => true, 'targetClass' => Sprints::className(), 'targetAttribute' => ['sprint' => 'id']],
            [['stage'], 'exist', 'skipOnError' => true, 'targetClass' => StagesTasks::className(), 'targetAttribute' => ['stage' => 'id']],
            [['types_directory'], 'exist', 'skipOnError' => true, 'targetClass' => Type::className(), 'targetAttribute' => ['types_directory' => 'id']],
            [['task_title', 'executor', 'manager_project', 'project', 'sprint', 'stage'], 'required', 'on' => self::SCENARIO_BASE],
            [['time_scheduled', 'time_fact', 'start_date', 'date_delivery'], 'required', 'on' => self::SCENARIO_SECOND],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author' => 'Автор',
            'manager_project' => 'Ответственный', //'Менеджер проекта',
            'executor' => 'Исполнитель',
            'priority' => 'Справочник типов',
            'project' => 'Проект',
            'stage' => 'Этап',
            'sprint' => 'Спринт',
            'types_directory' => 'Приоритет',
            'commentary' => 'Комментарии',
            'file' => 'Файл',
            'description' => 'Описание',
            'date_delivery' => 'Дата сдачи',
            'time_fact' => 'Время по факту',
            'time_scheduled' => 'Время запланировано',
            'task_title' => 'Название задачи',
            'tempFiles' => 'Файлы',
            'start_date' => 'Дата начало',
        ];
    }

    public function addFiles($tempFiles, $order_id) {
        if(is_null($tempFiles)) {
            return false;
        }
        foreach ($tempFiles as $itemFile) {
            $file = new Tasksfile();
            $file->order_id = $order_id;
            $file->title = $itemFile['name'];
            $file->path = $itemFile['path'];
            $file->save();
            unset($file);
        }
    }

    /**
     * @return bool
     */
    /*public function beforeDelete()
    {
        //$this->unlinkAll('priority0', true);

        return parent::beforeDelete();
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject0()
    {
        return $this->hasOne(Project::className(), ['id' => 'project']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor0()
    {
        return $this->hasOne(User::className(), ['id' => 'author']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutor0()
    {
        return $this->hasOne(User::className(), ['id' => 'executor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManagerProject()
    {
        return $this->hasOne(User::className(), ['id' => 'manager_project']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriority0()
    {
        return $this->hasOne(Priority::className(), ['id' => 'priority']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSprint0()
    {
        return $this->hasOne(Sprints::className(), ['id' => 'sprint']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStage0()
    {
        return $this->hasOne(StagesTasks::className(), ['id' => 'stage']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypesDirectory()
    {
        return $this->hasOne(Type::className(), ['id' => 'types_directory']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasksfiles()
    {
        return $this->hasMany(Tasksfile::className(), ['order_id' => 'id']);
    }
    public function getEtap()
    {
        $projects = Project::find()->all();
        $result = [];
        foreach ($projects as $value) {
            $sprint = Sprints::find()->where(['communications_project' => $value->id])->all();
            foreach ($sprint as $value1) 
            {
                $result[$value->name][$value1->name] = [];
                $stages = StagesTasks::find()->where(['communication_id' => $value->id])->all();
                foreach ($stages as $stage) {
                    $stepId = $stage->id;
                    $title = $stage->name;
                    $result[$value->name][$value1->name][$stepId] = $title;
                }
            }
            
        }

        return $result;
    }
    public function getColor()
    {
        if($this->typesDirectory->color == 1) return '#FF0000';
        if($this->typesDirectory->color == 2) return '#00FF00';
        if($this->typesDirectory->color == 3) return '#0000FF';
        if($this->typesDirectory->color == 4) return '#FFFF00';
        if($this->typesDirectory->color == 5) return '#000000';
        if($this->typesDirectory->color == 6) return '#FFFFFF';
        if($this->typesDirectory->color == 7) return '#444007';

        return '#FFFFFF';
    }

    public function getIconname()
    {
        /*echo "<pre>";
        print_r($this->priority);
        echo "</pre>";
        echo "g=".$this->priority->icon;die;*/
        /*if($this->priority == 1) return 'glyphicon glyphicon-ok';
        if($this->priority == 2) return 'glyphicon glyphicon-plus';
        if($this->priority == 3) return 'glyphicon glyphicon-minus';
        if($this->priority == 4) return 'glyphicon glyphicon-pencil';
        if($this->priority == 5) return 'glyphicon glyphicon-remove';
        if($this->priority == 6) return 'glyphicon glyphicon-trash';
        if($this->priority == 7) return 'glyphicon glyphicon-time';*/

        if($this->priority0->icon == 1)  return 'glyphicon glyphicon-fire';
        if($this->priority0->icon == 2)  return 'glyphicon glyphicon-asterisk';
        if($this->priority0->icon == 3)  return 'glyphicon glyphicon-cloud';
        if($this->priority0->icon == 4)  return 'glyphicon glyphicon-star';
        if($this->priority0->icon == 5)  return 'glyphicon glyphicon-trash';
        if($this->priority0->icon == 6)  return 'glyphicon glyphicon-time';
        if($this->priority0->icon == 7)  return 'glyphicon glyphicon-flash';
        if($this->priority0->icon == 8)  return 'glyphicon glyphicon-tag';
        if($this->priority0->icon == 9)  return 'glyphicon glyphicon-flag';
        if($this->priority0->icon == 10) return 'glyphicon glyphicon-tags';
        if($this->priority0->icon == 11) return 'glyphicon glyphicon-bookmark';
        if($this->priority0->icon == 12) return 'glyphicon glyphicon-camera';
        if($this->priority0->icon == 13) return 'glyphicon glyphicon-print';
        if($this->priority0->icon == 14) return 'glyphicon glyphicon-picture';
        if($this->priority0->icon == 15) return 'glyphicon glyphicon-share';
        if($this->priority0->icon == 16) return 'glyphicon glyphicon-eye-close';
        if($this->priority0->icon == 17) return 'glyphicon glyphicon-eye-open';
        if($this->priority0->icon == 18) return 'glyphicon glyphicon-warning-sign';
        if($this->priority0->icon == 19) return 'glyphicon glyphicon-comment';
        if($this->priority0->icon == 20) return 'glyphicon glyphicon-bullhorn';
        if($this->priority0->icon == 21) return 'glyphicon glyphicon-thumbs-up';
        if($this->priority0->icon == 22) return 'glyphicon glyphicon-thumbs-down';
        if($this->priority0->icon == 23) return 'glyphicon glyphicon-wrench';
        if($this->priority0->icon == 24) return 'glyphicon glyphicon-briefcase';
        if($this->priority0->icon == 25) return 'glyphicon glyphicon-dashboard';
        if($this->priority0->icon == 26) return 'glyphicon glyphicon-pushpin';
        if($this->priority0->icon == 27) return 'glyphicon glyphicon-send';
        if($this->priority0->icon == 28) return 'glyphicon glyphicon-ruble';
        if($this->priority0->icon == 29) return 'glyphicon glyphicon-piggy-bank';
        if($this->priority0->icon == 30) return 'glyphicon glyphicon-education';
    }

    public function getLastId()
    {
        $maxId = (new \yii\db\Query())
            ->select('auto_increment')
            ->from('information_schema.tables')
            ->where(['table_name' => 'tasks'])
            ->one();
        return $maxId['auto_increment'];
    }

    public function SendSmsToVk($id, $message)
    {
//        $ch = curl_init('https://api.telegram.org/bot478769636:AAHP29Ly-nHMwxe48wVG7H26iJDraclOG9g/sendMessage?chat_id='.$chat_id.'&parse_mode=html&text='.$message);
        // устанавлваем даные для отправки
//        curl_setopt($ch, CURLOPT_POSTFIELDS, '11111');
        // флаг о том, что нужно получить результат
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // отправляем запрос
//        $response = curl_exec($ch);
        // закрываем соединение
//        curl_close($ch);


        Yii::$app->vk->setTo($id)->setMessage($message)->send();
        
        // var_export($response);
        //echo "true";
    }

    public function Change($last, $new)
    {
        $result = ""; 

        if($last->author != $new->author) $result .= "Автор =>".$new->author."%0A";
        if($last->manager_project != $new->manager_project) $result .= 'Менеджер проекта => '. $new->managerProject->fio."%0A";
        if($last->executor != $new->executor) $result .= 'Исполнитель => '. $new->executor0->fio."%0A";
        if($last->priority != $new->priority) $result .= 'Приоритет => '. $new->priority0->name."%0A";
        if($last->project != $new->project) $result .= 'Проект => '. $new->project0->name."%0A";
        if($last->stage != $new->stage) $result .= 'Этап => '. $new->stage0->name."%0A";
        if($last->sprint != $new->sprint) $result .= 'Спринт => '. $new->sprint0->name."%0A";
        if($last->types_directory != $new->types_directory) $result .= 'Справочник типов => '. $new->typesDirectory->name."%0A";
        if($last->description != $new->description) $result .= 'Описание => '. HtmlPurifier::process($new->description, ['Attr.EnableID' => true,])."%0A";
        if($last->date_delivery != $new->date_delivery) $result .= 'Дата сдачи => '. $new->date_delivery."%0A";
        if($last->time_fact != $new->time_fact) $result .= 'Время по факту => '. $new->time_fact."%0A";
        if($last->time_scheduled != $new->time_scheduled) $result .= 'Время запланировано => '. $new->time_scheduled."%0A";
        if($last->task_title != $new->task_title) $result .= 'Название задачи => '. $new->task_title."%0A";
        /*'file' => 'Файл',
        'commentary' => 'Комментарии',*/
        return $result;
    }

    public function setChanging($old, $new, $status)
    {
        if($status == "Создано"){

            $this->setToChangeTable('Автор',"",$new->author0->fio,$status);
            $this->setToChangeTable('Исполнитель',"",$new->executor0->fio,$status);
            $this->setToChangeTable('Приоритет',"",$new->priority0->name,$status);
            $this->setToChangeTable('Проект',"",$new->project0->name,$status);
            $this->setToChangeTable('Этап',"",$new->stage0->name,$status);
            $this->setToChangeTable('Спринт',"",$new->sprint0->name,$status);
            $this->setToChangeTable('Справочник типов',"",$new->typesDirectory->name,$status);
            $this->setToChangeTable('Описание',HtmlPurifier::process("", ['Attr.EnableID' => true,]),HtmlPurifier::process($new->description, ['Attr.EnableID' => true,]),$status);
            $this->setToChangeTable('Дата сдачи',"",$new->date_delivery,$status);
            $this->setToChangeTable('Время по факту',"",$new->time_fact,$status);
            $this->setToChangeTable('Время запланировано',"",$new->time_scheduled,$status);
            $this->setToChangeTable('Название задачи',"",$new->task_title,$status);
        }else{

            if($old->author != $new->author) 
                $this->setToChangeTable('Автор',$old->author0->fio,$new->author0->fio,$status);
            if($old->manager_project != $new->manager_project) 
                $this->setToChangeTable('Менеджер проекта',$old->managerProject->fio,$new->managerProject->fio,$status);
            if($old->executor != $new->executor) 
                $this->setToChangeTable('Исполнитель',$old->executor0->fio,$new->executor0->fio,$status);
            if($old->priority != $new->priority) 
                $this->setToChangeTable('Приоритет',$old->priority0->name,$new->priority0->name,$status);
            if($old->project != $new->project) 
                $this->setToChangeTable('Проект',$old->project0->name,$new->project0->name,$status);
            if($old->stage != $new->stage) 
                $this->setToChangeTable('Этап',$old->stage0->name,$new->stage0->name,$status);
            if($old->sprint != $new->sprint)
                $this->setToChangeTable('Спринт',$old->sprint0->name,$new->sprint0->name,$status);
            if($old->types_directory != $new->types_directory) 
                $this->setToChangeTable('Справочник типов',$old->typesDirectory->name,$new->typesDirectory->name,$status);
            if($old->description != $new->description) 
                $this->setToChangeTable('Описание',HtmlPurifier::process($old->description, ['Attr.EnableID' => true,]),HtmlPurifier::process($new->description, ['Attr.EnableID' => true,]),$status);
            if($old->date_delivery != $new->date_delivery) $this->setToChangeTable('Дата сдачи',$old->date_delivery,$new->date_delivery,$status);
            if($old->time_fact != $new->time_fact) $this->setToChangeTable('Время по факту',$old->time_fact,$new->time_fact,$status);
            if($old->time_scheduled != $new->time_scheduled) $this->setToChangeTable('Время запланировано',$old->time_scheduled,$new->time_scheduled,$status);
            if($old->task_title != $new->task_title) $this->setToChangeTable('Название задачи',$old->task_title,$new->task_title,$status);
        }
        
    }
    public function setToChangeTable($field,$old_value,$new_value,$status)
    {
        $old_value = $old_value.'';
        $new_value = $new_value.'';
        $model = new Changing();
        $model->table_name = 'task';
        $model->line_id = $this->id;
        $model->date_time = date('Y-m-d H:i:s');
        $model->user_id = Yii::$app->user->identity->id;
        $model->fields = $field;
        $model->old_value = $old_value;
        $model->new_value = $new_value;
        $model->status = $status;
        $model->save();
    }

}
