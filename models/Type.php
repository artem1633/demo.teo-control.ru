<?php

namespace app\models;

use app\base\AppActiveQuery;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "type".
 *
 * @property integer $id
 * @property string $name
 * @property string $color
 *
 * @property Tasks[] $tasks
 */
class Type extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'color'], 'required'],
            [['name', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'color' => 'Цвет',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Tasks::className(), ['types_directory' => 'id']);
    }

    public function getColor()
    {
        return ArrayHelper::map([
            ['id' => '1',  'title' => 'Красный',],
            ['id' => '2',  'title' => 'Зелёный',],
            ['id' => '3',  'title' => 'Синий',],
            ['id' => '4',  'title' => 'Жёлтый',],
            ['id' => '5',  'title' => 'Чёрный',],
            ['id' => '6',  'title' => 'Белый',],
            ['id' => '7',  'title' => 'Коричневый',],
        ], 'id', 'title');
    }

    public function getColorDescription()
    {
        if($this->color == 1) return '#FF0000';
        if($this->color == 2) return '#00FF00';
        if($this->color == 3) return '#0000FF';
        if($this->color == 4) return '#FFFF00';
        if($this->color == 5) return '#000000';
        if($this->color == 6) return '#FFFFFF';
        if($this->color == 7) return '#444007';
    }
}
