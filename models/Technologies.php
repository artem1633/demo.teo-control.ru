<?php

namespace app\models;

use app\base\AppActiveQuery;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "technologies".
 *
 * @property integer $id
 * @property string $title
 */
class Technologies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'technologies';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'created_at' => 'Создал',
            'updated_at' => 'Изменил',
        ];
    }

    /**
     * Get select techologies titles by json string
     *
     * @param $json string Json string
     * @return \yii\db\ActiveRecord[]
     */
    public function getByJson($json)
    {
        $arr = json_decode($json, true);
        $arr = $this->find()->where(['id' => $arr])->asArray()->all();
        $res = [];
        foreach ($arr as $item) {
            array_push($res, $item['id']);
        }

        return $res;
    }

    /**
     * Get techologies titles by json string. Static method
     *
     * @param $json string Json string
     * @return \yii\db\ActiveRecord[]
     */
    public static function getByJsonStatic($json)
    {
        $arr = json_decode($json, true);
        $arr = static::find()->where(['id' => $arr])->asArray()->all();
        $res = [];
        foreach ($arr as $item) {
            array_push($res, $item['id']);
        }

        return $res;
    }

    /**
     * Return all technologies
     *
     * @return \yii\db\ActiveRecord[]
     */
    public function getAll()
    {
        return $this->find()->select(['title', 'id'])->indexBy('id')->column();
    }

    /**
     * Return titles fot select technologies
     *
     * @param $arr array Select technologies id
     * @return array
     */
    public static function getTitles($arr)
    {
        return self::find()->select(['title', 'id'])->where(['id' => $arr])->indexBy('id')->column();
    }
}
