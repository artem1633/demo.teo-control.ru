<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "companies".
 *
 * @property int $id
 * @property int $admin_id Id администратора компании
 * @property string $access_end_datetime
 * @property int $rate_id Тариф
 * @property string $last_activity_datetime
 * @property integer $is_super
 *
 * @property Changing[] $changings
 * @property Users $admin
 * @property Rates $rate
 * @property Tasks[] $tasks
 * @property Users[] $users
 */
class Companies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'companies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['admin_id'], 'integer'],
            [['admin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['admin_id' => 'id']],
            [['rate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rates::className(), 'targetAttribute' => ['rate_id' => 'id']],
            [['access_end_datetime'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'access_end_datetime' => 'Дата и время окончания доступа',
            'admin_id' => 'Администратор компании',
            'rate_id' => 'Тариф',
            'last_activity_datetime' => 'Последняя активность',
        ];
    }

    /**
     * @return bool
     */
    public function isSuperCompany()
    {
        return $this->is_super == 1 ? true : false;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if($this->isSuperCompany() === false)
        {
            return parent::beforeDelete();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCallings()
    {
        return $this->hasMany(Calling::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChangings()
    {
        return $this->hasMany(Changing::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStagesTasks()
    {
        return $this->hasMany(Changing::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(Users::className(), ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaqs()
    {
        return $this->hasMany(Faq::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPays()
    {
        return $this->hasMany(Pays::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelations()
    {
        return $this->hasMany(Relations::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSources()
    {
        return $this->hasMany(Source::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatuses()
    {
        return $this->hasMany(Statuses::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSprints()
    {
        return $this->hasMany(Sprints::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriority()
    {
        return $this->hasMany(Priority::className(), ['company_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskStatuses()
    {
        return $this->hasMany(TaskStatuses::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Tasks::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRate()
    {
        return $this->hasOne(Rates::className(), ['id' => 'rate_id']);
    }

    public function setDefaultValues()
    {
        $type = new Type();
        $type->name = 'Важно и Срочно';
        $type->color = '1';
        $type->save();

        $type = new Type();
        $type->name = 'Важно но не Срочно';
        $type->color = '2';
        $type->save();

        $type = new Type();
        $type->name = 'Не важно не срочно';
        $type->color = '4';
        $type->save();


        $priority = new Priority();
        $priority->name = 'Новый функционал';
        $priority->icon = '9';
        $priority->save();

        $priority = new Priority();
        $priority->name = 'Доработки';
        $priority->icon = '23';
        $priority->save();

        $priority = new Priority();
        $priority->name = 'Баги';
        $priority->icon = '1';
        $priority->save();

        $priority = new Priority();
        $priority->name = 'Пожелания';
        $priority->icon = '5';
        $priority->save();


        $technology = new Technologies();
        $technology->title = 'JS';
        $technology->created_at = date('Y-m-d H:i:s');
        $technology->updated_at = date('Y-m-d H:i:s');
        $technology->save();

        $technology = new Technologies();
        $technology->title = 'PHP';
        $technology->created_at = date('Y-m-d H:i:s');
        $technology->updated_at = date('Y-m-d H:i:s');
        $technology->save();

        $technology = new Technologies();
        $technology->title = 'MIGRATE';
        $technology->created_at = date('Y-m-d H:i:s');
        $technology->updated_at = date('Y-m-d H:i:s');
        $technology->save();

        $technology = new Technologies();
        $technology->title = 'TELEGRAM';
        $technology->created_at = date('Y-m-d H:i:s');
        $technology->updated_at = date('Y-m-d H:i:s');
        $technology->save();

        $technology = new Technologies();
        $technology->title = 'VK';
        $technology->created_at = date('Y-m-d H:i:s');
        $technology->updated_at = date('Y-m-d H:i:s');
        $technology->save();

        $technology = new Technologies();
        $technology->title = 'Написание ТЗ';
        $technology->created_at = date('Y-m-d H:i:s');
        $technology->updated_at = date('Y-m-d H:i:s');
        $technology->save();

        $technology = new Technologies();
        $technology->title = 'Тестирование';
        $technology->created_at = date('Y-m-d H:i:s');
        $technology->updated_at = date('Y-m-d H:i:s');
        $technology->save();

        $project = new Project();
        $project->name = 'Тестовый проект';
        $project->creator_composer = Yii::$app->user->identity->id;
        $project->responsible = Yii::$app->user->identity->id;
        $project->status = '3';
        $project->executor = Yii::$app->user->identity->id;
        
        if($project->save())
        {
            $project->CreateStage($project->id);

            $sprint = new Sprints();
            $sprint->communications_project = $project->id;
            $sprint->creator = Yii::$app->user->identity->id;
            $sprint->name = 'Спринт1';
            $sprint->status = '2';
            if($sprint->save())
            {
                $task = new Tasks();
                $task->author = Yii::$app->user->identity->id;
                $task->manager_project = Yii::$app->user->identity->id;
                $task->executor = Yii::$app->user->identity->id;
                //$task->priority = 'sdf';
                $task->project = $project->id;
                $task->stage = StagesTasks::find()->where(['communication_id' => $project->id ])->one()->id;
                $task->sprint = $sprint->id;
                $task->date_delivery = date('Y-m-d');
                $task->time_fact = 1;
                $task->time_scheduled = 1;
                $task->task_title = 'Задача1';
                $task->start_date = date('Y-m-d');
                $task->priority = Priority::find()->one()->id;
                $task->types_directory = Type::find()->one()->id;
                $task->save();

                $task = new Tasks();
                $task->author = Yii::$app->user->identity->id;
                $task->manager_project = Yii::$app->user->identity->id;
                $task->executor = Yii::$app->user->identity->id;
                //$task->priority = 'sdf';
                $task->project = $project->id;
                $task->stage = StagesTasks::find()->where(['communication_id' => $project->id ])->one()->id;
                $task->sprint = $sprint->id;
                $task->date_delivery = date('Y-m-d');
                $task->time_fact = 1;
                $task->time_scheduled = 1;
                $task->task_title = 'Задача2';
                $task->start_date = date('Y-m-d');
                $task->priority = Priority::find()->one()->id;
                $task->types_directory = Type::find()->one()->id;
                $task->save();
            }

            $sprint = new Sprints();
            $sprint->communications_project = $project->id;
            $sprint->creator = Yii::$app->user->identity->id;
            $sprint->name = 'Спринт2';
            $sprint->status = '3';
            if($sprint->save())
            {
                $task = new Tasks();
                $task->author = Yii::$app->user->identity->id;
                $task->manager_project = Yii::$app->user->identity->id;
                $task->executor = Yii::$app->user->identity->id;
                //$task->priority = 'sdf';
                $task->project = $project->id;
                $task->stage = StagesTasks::find()->where(['communication_id' => $project->id ])->one()->id;
                $task->sprint = $sprint->id;
                $task->date_delivery = date('Y-m-d');
                $task->time_fact = 1;
                $task->time_scheduled = 1;
                $task->task_title = 'Задача1';
                $task->start_date = date('Y-m-d');
                $task->priority = Priority::find()->one()->id;
                $task->types_directory = Type::find()->one()->id;
                $task->save();

                $task = new Tasks();
                $task->author = Yii::$app->user->identity->id;
                $task->manager_project = Yii::$app->user->identity->id;
                $task->executor = Yii::$app->user->identity->id;
                //$task->priority = 'sdf';
                $task->project = $project->id;
                $task->stage = StagesTasks::find()->where(['communication_id' => $project->id ])->one()->id;
                $task->sprint = $sprint->id;
                $task->date_delivery = date('Y-m-d');
                $task->time_fact = 1;
                $task->time_scheduled = 1;
                $task->task_title = 'Задача2';
                $task->start_date = date('Y-m-d');
                $task->priority = Priority::find()->one()->id;
                $task->types_directory = Type::find()->one()->id;
                $task->save();
            }
        }        
    }
}
