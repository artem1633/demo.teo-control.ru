<?php

namespace app\models;

use app\base\AppActiveQuery;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "priority".
 *
 * @property integer $id
 * @property string $name
 * @property string $icon
 *
 * @property Tasks[] $tasks
 */
class Priority extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'priority';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'icon' => 'Иконка',
        ];
    }

    public function beforeDelete()
    {
        $this->unlinkAll('tasks', true);

        return parent::beforeDelete();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Tasks::className(), ['priority' => 'id']);
    }

    public function getIconname()
    {
        if($this->icon == 1)  return 'glyphicon glyphicon-fire';
        if($this->icon == 2)  return 'glyphicon glyphicon-asterisk';
        if($this->icon == 3)  return 'glyphicon glyphicon-cloud';
        if($this->icon == 4)  return 'glyphicon glyphicon-star';
        if($this->icon == 5)  return 'glyphicon glyphicon-trash';
        if($this->icon == 6)  return 'glyphicon glyphicon-time';
        if($this->icon == 7)  return 'glyphicon glyphicon-flash';
        if($this->icon == 8)  return 'glyphicon glyphicon-tag';
        if($this->icon == 9)  return 'glyphicon glyphicon-flag';
        if($this->icon == 10) return 'glyphicon glyphicon-tags';
        if($this->icon == 11) return 'glyphicon glyphicon-bookmark';
        if($this->icon == 12) return 'glyphicon glyphicon-camera';
        if($this->icon == 13) return 'glyphicon glyphicon-print';
        if($this->icon == 14) return 'glyphicon glyphicon-picture';
        if($this->icon == 15) return 'glyphicon glyphicon-share';
        if($this->icon == 16) return 'glyphicon glyphicon-eye-close';
        if($this->icon == 17) return 'glyphicon glyphicon-eye-open';
        if($this->icon == 18) return 'glyphicon glyphicon-warning-sign';
        if($this->icon == 19) return 'glyphicon glyphicon-comment';
        if($this->icon == 20) return 'glyphicon glyphicon-bullhorn';
        if($this->icon == 21) return 'glyphicon glyphicon-thumbs-up';
        if($this->icon == 22) return 'glyphicon glyphicon-thumbs-down';
        if($this->icon == 23) return 'glyphicon glyphicon-wrench';
        if($this->icon == 24) return 'glyphicon glyphicon-briefcase';
        if($this->icon == 25) return 'glyphicon glyphicon-dashboard';
        if($this->icon == 26) return 'glyphicon glyphicon-pushpin';
        if($this->icon == 27) return 'glyphicon glyphicon-send';
        if($this->icon == 28) return 'glyphicon glyphicon-ruble';
        if($this->icon == 29) return 'glyphicon glyphicon-piggy-bank';
        if($this->icon == 30) return 'glyphicon glyphicon-education';

    }
    public function getIcon()
    {
        return ArrayHelper::map([
            ['id' => '1',   'title' => 'glyphicon glyphicon-fire',],
            ['id' => '2',   'title' => 'glyphicon glyphicon-asterisk',],
            ['id' => '3',   'title' => 'glyphicon glyphicon-cloud',],
            ['id' => '4',   'title' => 'glyphicon glyphicon-star',],
            ['id' => '5',   'title' => 'glyphicon glyphicon-trash',],
            ['id' => '6',   'title' => 'glyphicon glyphicon-time',],
            ['id' => '7',   'title' => 'glyphicon glyphicon-flash',],
            ['id' => '8',   'title' => 'glyphicon glyphicon-tag',],
            ['id' => '9',   'title' => 'glyphicon glyphicon-flag',],
            ['id' => '10',  'title' => 'glyphicon glyphicon-tags',],
            ['id' => '11',  'title' => 'glyphicon glyphicon-bookmark',],
            ['id' => '12',  'title' => 'glyphicon glyphicon-camera',],
            ['id' => '13',  'title' => 'glyphicon glyphicon-print',],
            ['id' => '14',  'title' => 'glyphicon glyphicon-picture',],
            ['id' => '15',  'title' => 'glyphicon glyphicon-share',],
            ['id' => '16',  'title' => 'glyphicon glyphicon-eye-close',],
            ['id' => '17',  'title' => 'glyphicon glyphicon-eye-open',],
            ['id' => '18',  'title' => 'glyphicon glyphicon-warning-sign',],
            ['id' => '19',  'title' => 'glyphicon glyphicon-comment',],
            ['id' => '20',  'title' => 'glyphicon glyphicon-bullhorn',],
            ['id' => '21',  'title' => 'glyphicon glyphicon-thumbs-up',],
            ['id' => '22',  'title' => 'glyphicon glyphicon-thumbs-down',],
            ['id' => '23',  'title' => 'glyphicon glyphicon-wrench',],
            ['id' => '24',  'title' => 'glyphicon glyphicon-briefcase',],
            ['id' => '25',  'title' => 'glyphicon glyphicon-dashboard',],
            ['id' => '26',  'title' => 'glyphicon glyphicon-pushpin',],
            ['id' => '27',  'title' => 'glyphicon glyphicon-send',],
            ['id' => '28',  'title' => 'glyphicon glyphicon-ruble',],
            ['id' => '29',  'title' => 'glyphicon glyphicon-piggy-bank',],
            ['id' => '30',  'title' => 'glyphicon glyphicon-education',],
        ], 'id', 'title');
        //return ArrayHelper::map(Icons::find()->all(), 'id', 'title');
    }
}
