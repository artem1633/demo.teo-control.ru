<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class RegisterForm
 * @package app\models
 */
class RegisterForm extends Model
{
    const EVENT_REGISTERED = 'event_registered';

    public $fio;
    public $phone;
    public $login;
    public $password;
    public $rate_id;
    public $vk_href;

    /**
     * @var Users содержит созраненную модель users для возможности доступа к ней
     * через обработчик событий
     * @see \app\event_handlers\RegisteredFormEventHandler
     */
    public $_user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'class' => \app\event_handlers\RegisteredFormEventHandler::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'phone', 'login', 'rate_id', 'password'], 'required'],
            [['rate_id'], 'integer'],
            [['login'], 'email'],
            [['login'], 'unique', 'targetClass' => '\app\models\Users'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fio' => 'ФИО',
            'phone' => 'Телефон',
            'login' => 'E-mail',
            'password' => 'Пароль',
            'vk_href' => 'Ссылка на страницу ВК',
            'rate_id' => 'Тариф',
        ];
    }

    /**
     * Регистрирует нового пользователя
     * @param int $type
     * @return Users|null
     */
    public function register($type = Users::USER_TYPE_ADMIN)
    {
        if($this->validate() === false){
            return null;
        }

        $user = new Users();
        $user->attributes = $this->attributes;
        $dateNow = time();
        $rate = Rates::findOne($this->rate_id);
        if($rate != null){
            $accessEndDateTime = date('Y-m-d H:i:s', strtotime("+ {$rate->time} seconds", $dateNow));
        } else {
            $accessEndDateTime = date('Y-m-d H:i:s', strtotime("+ 14 days", $dateNow));
        }
        $user->type = $type;
        $user->dostup = 1;

        if($user->save())
        {
            $this->_user = $user;
            $company = new Companies([
                'admin_id' => $user->id,
                'access_end_datetime' => $accessEndDateTime,
                'rate_id' => $this->rate_id,
            ]);

            $company->save();

            $user->link('company', $company);

            $this->trigger(self::EVENT_REGISTERED);

            $template = EmailTemplates::findByKey('success_registration');

            if($template != null){
                $body = $template->applyTags($user);

                try {
                    Yii::$app->mailer->compose()
                        ->setFrom('zvonki.crm@mail.ru')
                        ->setTo($user->login)
                        ->setSubject('Успешная регистрация')
                        ->setHtmlBody($body)
                        ->send();
                } catch(\Exception $e){

                }
            }

            return $user;
        }

        return null;
    }

}