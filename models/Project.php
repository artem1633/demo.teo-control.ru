<?php

namespace app\models;

use app\base\AppActiveQuery;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $name
 * @property integer $creator_composer
 * @property integer $responsible
 * @property string $status
 *
 * @property Tasks[] $tasks
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'responsible', 'executor'], 'required'],
            [['creator_composer', 'responsible', 'client', 'is_delete', 'executor', 'sorting'], 'integer'],
            [['name', 'status'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['responsible'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['responsible' => 'id']],
            [['executor'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['responsible' => 'id']],
            [['creator_composer'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator_composer' => 'id']],
            [['client'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['client' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'creator_composer' => 'Кто создал',
            'responsible' => 'Ответственный',
            'status' => 'Статус',
            'client' => 'Клиент',
            'is_delete' => 'Удалено',
            'description' => 'Описания',
            'executor' => 'Исполнитель',
            'sorting' => 'Сортировка',
        ];
    }

    public function getStatus()
    {
        return ArrayHelper::map([
            ['id' => '1',  'title' => 'Готово',],
            ['id' => '2',  'title' => 'Планируется',],
            ['id' => '3',  'title' => 'в Работе',],
        ], 'id', 'title');
    
    }

    public function getDeleted()
    {
        return ArrayHelper::map([
            ['id' => '0',  'title' => 'Не удалено',],
            ['id' => '1',  'title' => 'Удалено',],
        ], 'id', 'title');
    
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsible0()
    {
        return $this->hasOne(User::className(), ['id' => 'responsible']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatorComposer()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_composer']);
    }

    public function getClient0()
    {
        return $this->hasOne(Users::className(), ['id' => 'client']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutor0()
    {
        return $this->hasOne(Users::className(), ['id' => 'executor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSprints()
    {
        return $this->hasMany(Sprints::className(), ['communications_project' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStagesTasks()
    {
        return $this->hasMany(StagesTasks::className(), ['communication_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Tasks::className(), ['project' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return array
     */
    public function ExecutorList()
    {
        $companyId = Yii::$app->user->identity->getCompany();
        $executor = Users::find()->where(['type' => 5])->andFilterWhere(['company_id' => $companyId])->all();
        return ArrayHelper::map($executor,'id','fio');
    }

    /**
     * @param $id
     */
    public function CreateStage($id)
    {
        /*$sprint = new Sprints();
        $sprint->name = 'Первый спринт';
        $sprint->communications_project = $id;
        $sprint->creator = Yii::$app->user->identity->id;
        $sprint->status = 2;
        $sprint->save();*/

        $stage = new StagesTasks();
        $stage->name = "Планируется";
        $stage->status = 1;
        $stage->sorting = 1;
        $stage->color = "3";
        $stage->show = "1";
        $stage->communication_id = $id;
        $stage->company_id = Yii::$app->user->identity->company_id;
        $stage->save();

        $stage = new StagesTasks();
        $stage->name = "В работе";
        $stage->status = 2;
        $stage->sorting = 2;
        $stage->color = "4";
        $stage->show = "1";
        $stage->communication_id = $id;
        $stage->company_id = Yii::$app->user->identity->company_id;
        $stage->save();

        $stage = new StagesTasks();
        $stage->name = "Тестирование";
        $stage->status = 2;
        $stage->sorting = 3;
        $stage->color = "1";
        $stage->show = "1";
        $stage->communication_id = $id;
        $stage->company_id = Yii::$app->user->identity->company_id;
        $stage->save();

        $stage = new StagesTasks();
        $stage->name = "Готово";
        $stage->status = 3;
        $stage->sorting = 4;
        $stage->color = "2";
        $stage->show = "1";
        $stage->communication_id = $id;
        $stage->company_id = Yii::$app->user->identity->company_id;
        $stage->save();

        $stage = new StagesTasks();
        $stage->name = "Оплачено";
        $stage->status = 3;
        $stage->sorting = 5;
        $stage->color = "5";
        $stage->show = "1";
        $stage->communication_id = $id;
        $stage->company_id = Yii::$app->user->identity->company_id;
        $stage->save();
    }
}
