<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;

/**
 * UsersSearch represents the model behind the search form about `app\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'dostup', 'cost', 'otsenka_opit', 'opit_god', 'creator'], 'integer'],
            [['fio', 'login', 'password', 'telephone', 'city', 'comment', 'technology', 'avatar', 'company_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find();

        if(Yii::$app->user->identity->isSuperAdmin() === false){
            $query = Users::find()->where(['company_id' => Yii::$app->user->identity->getCompany()]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'dostup' => $this->dostup,
            'cost' => $this->cost,
            'otsenka_opit' => $this->otsenka_opit,
            'opit_god' => $this->opit_god,
            'creator' => $this->creator,
        ]);

        $query->with('company');
        $query->with('company.admin');

        $query->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'technology', $this->technology]);

        if(Yii::$app->user->identity->isSuperAdmin())
        {
            $query->andFilterWhere(['company_id' => $this->company_id]);
        }

        return $dataProvider;
    }
}
