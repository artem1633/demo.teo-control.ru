<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Project;

/**
 * ProjectSearch represents the model behind the search form about `app\models\Project`.
 */
class ProjectSearch extends Project
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'creator_composer', 'responsible', 'client', 'executor', 'sorting'], 'integer'],
            [['name', 'status', 'company_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tip = Yii::$app->user->identity->type;
        $user = Users::findOne(Yii::$app->user->identity->id);
        $id = Yii::$app->user->identity->id;
        if($tip == 4)
        {
           $query = Project::find()->where(['company_id' => $user->company_id, 'client' => $id ]);
        }
        if($tip == 1 | $tip == 6 | $tip == 0)
        {
           $query = Project::find();
        }
        if($tip == 3)
        {
           $query = Project::find()->where(['company_id' => $user->company_id, 'responsible' => $id ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->with('company');
        $query->with('company.admin');

        $query->andFilterWhere([
            'id' => $this->id,
            'creator_composer' => $this->creator_composer,
            'responsible' => $this->responsible,
            'client' => $this->client,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'status', $this->status]);

        if(Yii::$app->user->identity->isSuperAdmin())
        {
            $query->andFilterWhere(['company_id' => $this->company_id]);
        }

        return $dataProvider;
    }
}
