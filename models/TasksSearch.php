<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tasks;

/**
 * TasksSearch represents the model behind the search form about `app\models\Tasks`.
 */
class TasksSearch extends Tasks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'author', 'manager_project', 'executor', 'priority', 'project', 'stage', 'sprint', 'types_directory', 'time_fact', 'time_scheduled'], 'integer'],
            [['commentary', 'file', 'description', 'date_delivery', 'task_title', 'company_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tip = Yii::$app->user->identity->type;


        if($tip == 0)
        {
            $query = Tasks::find();
        }
        if($tip == 5)
        {
            $query = Tasks::find()->where(['executor' => Yii::$app->user->identity->id]);
        }
        if($tip == 3)
        {
            $query = Tasks::find()->where(['manager_project' => Yii::$app->user->identity->id]);
        }
        if($tip == 1 | $tip == 6)
        {
            $query = Tasks::find();

        }
        if($tip == 4)
        {
            $tasks = Project::find()->where(['client' => Yii::$app->user->identity->id])->all();
            $result = [];
            foreach ($tasks as $value) {
                $result [] = $value->id;
            }
            $result = array_unique($result);
            $query = Tasks::find()->where(['project' => $result]);            
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->with('company');
        $query->with('company.admin');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'author' => $this->author,
            'manager_project' => $this->manager_project,
            'executor' => $this->executor,
            'priority' => $this->priority,
            'project' => $this->project,
            'stage' => $this->stage,
            'sprint' => $this->sprint,
            'types_directory' => $this->types_directory,
            'date_delivery' => $this->date_delivery,
            'time_fact' => $this->time_fact,
            'time_scheduled' => $this->time_scheduled,
        ]);

        $query->andFilterWhere(['like', 'commentary', $this->commentary])
            ->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'task_title', $this->task_title]);

        if(Yii::$app->user->identity->isSuperAdmin())
        {
            $query->andFilterWhere(['company_id' => $this->company_id]);
        }

        return $dataProvider;
    }
}
