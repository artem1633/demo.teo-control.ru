<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sprints;

/**
 * SprintsSearch represents the model behind the search form about `app\models\Sprints`.
 */
class SprintsSearch extends Sprints
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'communications_project', 'creator', 'status'], 'integer'],
            [['date_completion', 'date_start', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //$query = Sprints::find();
        $user = Users::findOne(Yii::$app->user->identity->id);
        $query = Sprints::find();
//        if($user->creator != null) $query = Sprints::find()->where(['company' => $user->creator ]);
//        else $query = Sprints::find()->where(['company' => Yii::$app->user->identity->id ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'communications_project' => $this->communications_project,
            'creator' => $this->creator,
            'date_completion' => $this->date_completion,
            'date_start' => $this->date_start,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
