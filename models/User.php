<?php

namespace app\models;

class User extends Users/*\yii\base\Object*/ implements \yii\web\IdentityInterface
{
   /* public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];*/


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Возвращает id компании, в которой состоит пользователь
     * @return int|null
     */
    public function getCompany()
    {
        return $this->company_id;
    }

    /**
     * Является ли пользователь супер администратором
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->type === Users::USER_TYPE_SUPER_ADMIN;
    }

    /**
     * Возвращает компанию
     * @return \app\models\Companies|null
     */
    public function getCompanyInstance()
    {
        return Companies::findOne($this->company_id);
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
       return $this->password === md5($password);
    }
}
