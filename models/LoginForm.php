<?php

namespace app\models;

use Yii;
use yii\base\Event;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    const EVENT_AUTHORIZED = 'event_authorized';

    public $username;
    public $password;
    public $rememberMe = true;

    public $_user = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => \app\event_handlers\LoginFormEventHandler::class,
            ]
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный логин или пароль.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $this->_user = $this->getUser();
            $this->trigger(self::EVENT_AUTHORIZED);
            return Yii::$app->user->login($this->_user, $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        if($this->_user->company->access_end_datetime === null || (new \DateTime($this->_user->company->access_end_datetime))->getTimestamp() > time() || $this->_user->company->isSuperCompany()){
            return $this->_user;
        } else {
            $this->addError('password', 'Срок действия лицензии истек');
        }
    }
}
