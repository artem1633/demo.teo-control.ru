<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "signup".
 *
 * @property integer $id
 * @property string $fio
 * @property string $email
 * @property string $type
 * @property string $password
 */
class Signup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'signup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'email', 'password'], 'required'],
            [['email'], 'email'],
            [['fio', 'email', 'type', 'password'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'email' => 'E-mail',
            'type' => ' Тип компании',
            'password' => 'Пароль',
        ];
    }
}
