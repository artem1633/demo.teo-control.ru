<?php

namespace app\models;

use app\base\AppActiveQuery;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "sprints".
 *
 * @property integer $id
 * @property string $communications_project
 * @property integer $responsible
 * @property integer $creator
 * @property string $date_completion
 * @property string $date_start
 * @property string $name
 *
 * @property Tasks[] $tasks
 */
class Sprints extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sprints';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'company_id',
                'updatedByAttribute' => null,
                'value' => function($event) {
                    return Yii::$app->user->identity->company_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        if(Yii::$app->user->isGuest == false){
            $companyId = Yii::$app->user->identity->getCompany();
        } else {
            $companyId = null;
        }

        return new AppActiveQuery(get_called_class(), [
            'companyId' => $companyId,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findOne($condition)
    {
        $model = parent::findOne($condition);
        if(Yii::$app->user->isGuest == false && isset($model->company_id)) {
            if(Yii::$app->user->identity->isSuperAdmin() === false)
            {
                $companyId = Yii::$app->user->identity->getCompany();
                if($model->company_id != $companyId){
                    throw new ForbiddenHttpException('Доступ запрещен');
                }
            }
        }
        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'creator', 'communications_project', 'status', 'is_delete'], 'integer'],
            [['date_completion', 'date_start'], 'safe'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['communications_project'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['communications_project' => 'id']],
            [['creator'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator' => 'id']],
            //[['responsible'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['responsible' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'communications_project' => 'Проект',
            //'responsible' => 'Ответственный',
            'creator' => 'Создатель',
            'date_completion' => 'Дата завершения',
            'date_start' => 'Дата начало',
            'name' => 'Название',
            'status' => 'Статус',
            'is_delete' => 'Удалено',
        ];
    }

   /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommunicationsProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'communications_project']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator0()
    {
        return $this->hasOne(User::className(), ['id' => 'creator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Tasks::className(), ['sprint' => 'id']);
    }
    public function getStatus()
    {
        return ArrayHelper::map([
            ['id' => '1',  'title' => 'Готово',],
            ['id' => '2',  'title' => 'Планируется',],
            ['id' => '3',  'title' => 'в Работе',],
        ], 'id', 'title');
    
    }

    public function getDeleted()
    {
        return ArrayHelper::map([
            ['id' => '0',  'title' => 'Не удалено',],
            ['id' => '1',  'title' => 'Удалено',],
        ], 'id', 'title');
    
    }
}
